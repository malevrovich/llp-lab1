import matplotlib.pyplot as plt
import seaborn as sns

FILENAME = "create_delete_match.txt"

def parse_file():
  f = open(FILENAME)
  elems, time = [], []

  f.readline()
  f.readline()
  line = f.readline()

  while line:
    for i in range(5):
      f.readline()
    s = line.split()
    elems.append(int(s[3]))
    time.append(float(s[-2]))
    line = f.readline()
  return elems, time

sns.set_theme()
xs, ys = parse_file()
plt.plot(xs, ys, marker='o')
plt.xticks(xs)
plt.title("Half of storage deleted")
plt.xlabel("Size of storage")
plt.ylabel("Time, s")
plt.savefig("delete_linear.png")