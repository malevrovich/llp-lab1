import matplotlib.pyplot as plt
import seaborn as sns

FILENAME = "create.txt"

def parse_file():
  f = open(FILENAME, "r+")
  elems = []
  time = []
  for line in f:
    elems.append(int(line.split()[1]))
    if len(elems) > 1:
      elems[-1] += elems[-2]
    time.append(float(line.split()[-2]))
  return elems, time

sns.set_theme()
xs, ys = parse_file()
plt.plot(xs, ys, marker="o")
plt.xlabel("Number of elements")
plt.ylabel(f"Time of creating {xs[0]} elems, s")
plt.savefig("create.png")