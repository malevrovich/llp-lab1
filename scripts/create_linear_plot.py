import matplotlib.pyplot as plt
import seaborn as sns

FILENAME = "create_delete_match.txt"

def parse_file():
  f = open(FILENAME)
  elems, time, time2 = [], [], []

  line = f.readline()
  while line:
    s = line.split()
    elems.append(int(s[1]))
    time.append(float(s[-2]))
    f.readline()
    f.readline()
    line = f.readline()
    time2.append(float(line.split()[-2]))
    f.readline()
    f.readline()
    line = f.readline()
  return elems, time, time2

sns.set_theme()
xs, ys, ys2 = parse_file()
plt.plot(xs, ys, marker='o', label='empty file')
plt.plot(xs, ys2, marker='o', label='after delete')
plt.legend()
plt.xticks(xs)
plt.xlabel("Number of elements created")
plt.ylabel("Time, s")
plt.savefig("create_linear.png")