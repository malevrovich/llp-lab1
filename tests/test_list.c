#include "offset_list_extended_interface.h"
#include "test_utils.h"

#include "offset_test_allocator_interface.h"
#include "offset_test_memory_manager_interface.h"

#include <stdio.h>

#define CHECK_INT_ITER_VALUE(iter, expected)                                \
  do {                                                                      \
    int real;                                                               \
    offset_list_iterator_read_data(iter, (mem_size_t){sizeof(int)}, &real); \
    if (real != expected) {                                                 \
      printf("Expected: %d, real: %d\n", expected, real);                   \
    }                                                                       \
    CHECK(real == expected);                                                \
  } while (0)

TEST(simple) {
  struct offset_fixed_base_allocator* alloc;
  offset_test_allocator_alloc((struct offset_test_allocator**)&alloc);

  struct offset_memory_manager* mem_manager;
  offset_test_memory_manager_alloc((struct offset_test_memory_manager**)&mem_manager);

  mem_size_t int_size = {sizeof(int)};

  offset_test_allocator_ctor((struct offset_test_allocator*)alloc, offset_list_get_node_size(int_size));
  offset_test_memory_manager_ctor((struct offset_test_memory_manager*)mem_manager, (mem_size_t){0});

  struct offset_fixed_base_allocator* header_alloc;
  offset_test_allocator_alloc((struct offset_test_allocator**)&header_alloc);
  offset_test_allocator_ctor((struct offset_test_allocator*)header_alloc, offset_list_get_data_size());

  mem_offset_t list_offset = offset_fixed_base_allocator_allocate((struct offset_fixed_base_allocator*)header_alloc);

  struct offset_list* list;
  offset_list_alloc(&list);
  offset_list_ctor(list, list_offset, (struct offset_memory_manager*)mem_manager,
                   (struct offset_fixed_base_allocator*)alloc);

  CHECK(offset_list_empty(list));

  int f_expected = 1;
  offset_list_push_back(list, int_size, &f_expected);

  CHECK(!offset_list_empty(list));

  struct offset_list_iterator* head_iter;
  offset_list_iterator_alloc(&head_iter);
  offset_list_head(list, head_iter);

  CHECK_INT_ITER_VALUE(head_iter, f_expected);

  offset_list_iterator_next(head_iter);

  CHECK_INT_ITER_VALUE(head_iter, 1);

  offset_list_pop_back(list);
  CHECK(offset_list_empty(list));

  int list_data[] = {2, 3, 4, -1, 10};
  for (int i = 0; i < 5; i++) {
    offset_list_push_back(list, int_size, &list_data[i]);
  }

  offset_list_head(list, head_iter);

  for (int i = 0; i < 5; i++, offset_list_iterator_next(head_iter)) {
    CHECK_INT_ITER_VALUE(head_iter, list_data[i]);
  }

  offset_list_head(list, head_iter);
  offset_list_iterator_next(head_iter);
  offset_list_delete(list, head_iter);

  offset_list_head(list, head_iter);
  CHECK_INT_ITER_VALUE(head_iter, 2);
  offset_list_iterator_next(head_iter);
  CHECK_INT_ITER_VALUE(head_iter, 4);
  offset_list_iterator_next(head_iter);
  CHECK_INT_ITER_VALUE(head_iter, -1);
  offset_list_iterator_next(head_iter);
  CHECK_INT_ITER_VALUE(head_iter, 10);
  offset_list_iterator_next(head_iter);
  CHECK_INT_ITER_VALUE(head_iter, 2);

  offset_list_pop_back(list);

  offset_list_head(list, head_iter);
  CHECK_INT_ITER_VALUE(head_iter, 2);
  offset_list_iterator_next(head_iter);
  CHECK_INT_ITER_VALUE(head_iter, 4);
  offset_list_iterator_next(head_iter);
  CHECK_INT_ITER_VALUE(head_iter, -1);
  offset_list_iterator_next(head_iter);
  CHECK_INT_ITER_VALUE(head_iter, 2);
  offset_list_iterator_next(head_iter);

  offset_list_pop_back(list);

  offset_list_head(list, head_iter);
  CHECK_INT_ITER_VALUE(head_iter, 2);
  offset_list_iterator_next(head_iter);
  CHECK_INT_ITER_VALUE(head_iter, 4);
  offset_list_iterator_next(head_iter);
  CHECK_INT_ITER_VALUE(head_iter, 2);

  offset_list_iterator_next(head_iter);
  offset_list_delete(list, head_iter);

  offset_list_head(list, head_iter);
  CHECK_INT_ITER_VALUE(head_iter, 2);
  offset_list_iterator_next(head_iter);
  CHECK_INT_ITER_VALUE(head_iter, 2);

  offset_list_push_back(list, int_size, &list_data[1]);
  offset_list_push_back(list, int_size, &list_data[2]);

  offset_list_head(list, head_iter);
  CHECK_INT_ITER_VALUE(head_iter, 2);
  offset_list_iterator_next(head_iter);
  CHECK_INT_ITER_VALUE(head_iter, 3);
  offset_list_iterator_next(head_iter);
  CHECK_INT_ITER_VALUE(head_iter, 4);
  offset_list_iterator_next(head_iter);
  CHECK_INT_ITER_VALUE(head_iter, 2);

  offset_list_iterator_next(head_iter);
  offset_list_delete(list, head_iter);

  offset_list_head(list, head_iter);
  CHECK_INT_ITER_VALUE(head_iter, 2);
  offset_list_iterator_next(head_iter);
  CHECK_INT_ITER_VALUE(head_iter, 4);
  offset_list_iterator_next(head_iter);
  CHECK_INT_ITER_VALUE(head_iter, 2);

  offset_list_pop_back(list);
  offset_list_pop_back(list);

  CHECK(offset_list_empty(list));

  for (int i = 0; i < 5; i++) {
    offset_list_push_back(list, int_size, &list_data[i]);
  }

  offset_list_pop_front(list);
  offset_list_head(list, head_iter);
  CHECK_INT_ITER_VALUE(head_iter, 3);
  offset_list_iterator_next(head_iter);
  CHECK_INT_ITER_VALUE(head_iter, 4);
  offset_list_iterator_next(head_iter);
  CHECK_INT_ITER_VALUE(head_iter, -1);
  offset_list_iterator_next(head_iter);
  CHECK_INT_ITER_VALUE(head_iter, 10);
  offset_list_iterator_next(head_iter);
  CHECK_INT_ITER_VALUE(head_iter, 3);

  offset_list_dtor(list);
  offset_list_ctor_from_place(list, list_offset, mem_manager, alloc);

  offset_list_head(list, head_iter);
  CHECK_INT_ITER_VALUE(head_iter, 3);
  offset_list_iterator_next(head_iter);
  CHECK_INT_ITER_VALUE(head_iter, 4);
  offset_list_iterator_next(head_iter);
  CHECK_INT_ITER_VALUE(head_iter, -1);
  offset_list_iterator_next(head_iter);
  CHECK_INT_ITER_VALUE(head_iter, 10);
  offset_list_iterator_next(head_iter);
  CHECK_INT_ITER_VALUE(head_iter, 3);

  offset_list_iterator_next(head_iter);
  offset_list_delete(list, head_iter);

  offset_list_head(list, head_iter);
  CHECK_INT_ITER_VALUE(head_iter, 3);
  offset_list_iterator_next(head_iter);
  CHECK_INT_ITER_VALUE(head_iter, -1);
  offset_list_iterator_next(head_iter);
  CHECK_INT_ITER_VALUE(head_iter, 10);
  offset_list_iterator_next(head_iter);
  CHECK_INT_ITER_VALUE(head_iter, 3);

  offset_list_pop_back(list);
  offset_list_pop_front(list);

  offset_list_head(list, head_iter);
  CHECK_INT_ITER_VALUE(head_iter, -1);
  offset_list_iterator_next(head_iter);
  CHECK_INT_ITER_VALUE(head_iter, -1);

  offset_list_push_front(list, int_size, &list_data[0]);

  offset_list_head(list, head_iter);
  CHECK_INT_ITER_VALUE(head_iter, 2);
  offset_list_iterator_next(head_iter);
  CHECK_INT_ITER_VALUE(head_iter, -1);
  offset_list_iterator_next(head_iter);
  CHECK_INT_ITER_VALUE(head_iter, 2);

  offset_list_pop_back(list);
  offset_list_pop_front(list);

  CHECK(offset_list_empty(list));
  offset_list_dtor(list);
  offset_list_ctor_from_place(list, list_offset, mem_manager, alloc);
  CHECK(offset_list_empty(list));
  offset_list_dtor(list);
  free(list);

  free(head_iter);
  offset_fixed_base_allocator_free(header_alloc, list_offset);
  offset_test_memory_manager_dtor((struct offset_test_memory_manager*)mem_manager);
  free(header_alloc);
  free(mem_manager);
  free(alloc);
}

int main() {
#ifndef DISABLED
  RUN_TEST(simple);
#else
  printf("Test disabled\n");
#endif /* DISABLED */
  return 0;
}