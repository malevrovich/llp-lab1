#include "query_processor.h"

#include "element_check.h"
#include "query.h"
#include "test_utils.h"

#include <stdio.h>
#include <time.h>

#define COUNT 200000

TEST(create_match_update_close_match) {
  struct element_storage* storage;
  CHECK(element_storage_alloc(&storage));

  CHECK(element_storage_ctor(storage, "test.txt", 4096));

  enum ATTRIBUTE_TYPE attrs_type1[5] = {ATTR_TYPE_BOOL, ATTR_TYPE_DOUBLE, ATTR_TYPE_INT32, ATTR_TYPE_INT64,
                                        ATTR_TYPE_STRING};
  struct element_type type1 = {.type_id = 0, .type_name = "Full", .attribute_count = 5, .attribute_types = attrs_type1};

  const int name_size = 8192;
  char type2_name[name_size];
  for (int i = 0; i < name_size - 1; i++) {
    type2_name[i] = 'o';
  }
  type2_name[name_size - 1] = '\0';

  enum ATTRIBUTE_TYPE attrs_type2[2] = {ATTR_TYPE_BOOL, ATTR_TYPE_INT32};
  struct element_type type2 = {
      .type_id = 1, .type_name = type2_name, .attribute_count = 2, .attribute_types = attrs_type2};

  CHECK(element_storage_add_element_type(storage, &type1, NULL) == 0);
  CHECK(element_storage_add_element_type(storage, &type2, NULL) == 0);

  char* init_string = "VALUE";
  struct attribute attrs1[5] = {{.type = ATTR_TYPE_BOOL, .value.as_bool = false},
                                {.type = ATTR_TYPE_DOUBLE, .value.as_double = 0.25},
                                {.type = ATTR_TYPE_INT32, .value.as_int32 = -1},
                                {.type = ATTR_TYPE_INT64, .value.as_int64 = 1},
                                {.type = ATTR_TYPE_STRING, .value.as_string = init_string}};
  struct element el_type1 = {.element_type = &type1, .attribute_count = 5, .attributes = attrs1};

  struct attribute attrs2[2] = {{.type = ATTR_TYPE_BOOL, .value.as_bool = true},
                                {.type = ATTR_TYPE_INT32, .value.as_int32 = 0}};

  struct element el_type2 = {.element_type = &type2, .attribute_count = 2, .attributes = attrs2};

  struct query q = {.type = QUERY_TYPE_CREATE};
  clock_t start = clock();
  for (int i = 0; i < COUNT; i++) {
    attrs1[3].value.as_int64 = i % 10 < 5 ? INT64_MIN : INT64_MAX;

    q.args.as_create.element = &el_type1;
    CHECK(process_query(storage, &q, NULL) == 0);

    attrs2[0].value.as_bool = (i % 7 != 0);
    q.args.as_create.element = &el_type2;
    CHECK(process_query(storage, &q, NULL) == 0);
  }
  clock_t end = clock();
  double elapsed_time = (end - start) / (double)CLOCKS_PER_SEC;
  printf("Created %d elements within %lf seconds\n", COUNT * 2, elapsed_time);

  struct attribute_pattern attr_pattern1 = {
      .node_type = ATTRIBUTE_PATTERN_COND,
      .args.as_cond = {.cond = ATTRIBUTE_PATTERN_COND_EQ,
                       .attr_id = 3,
                       .attr = {.type = ATTR_TYPE_INT64, .value.as_int64 = INT64_MAX}}};

  struct element_pattern el_pattern1 = {.attr_pattern = &attr_pattern1, .element_type = &type1};

  struct attribute_pattern attr_pattern2 = {
      .node_type = ATTRIBUTE_PATTERN_COND,
      .args.as_cond = {
          .cond = ATTRIBUTE_PATTERN_COND_EQ, .attr_id = 0, .attr = {.type = ATTR_TYPE_BOOL, .value.as_bool = false}}};

  struct element_pattern el_pattern2 = {.attr_pattern = &attr_pattern2, .element_type = &type2};

  int match1 = 0;
  q.type = QUERY_TYPE_MATCH;
  q.args.as_match.pattern = &el_pattern1;

  struct query_result* q_res;
  CHECK(query_result_alloc(&q_res));

  struct element* real;
  start = clock();
  error_code error = process_query(storage, &q, q_res);
  while (error == 0) {
    match1++;
    CHECK(query_result_get_data(q_res, &real) == 0);

    CHECK(is_type_equals(real->element_type, &type1));
    CHECK(real->attributes[3].value.as_int64 == INT64_MAX);

    error = query_result_next(q_res);
    element_dtor(real);
    free(real);
  }
  CHECK(error == ERROR_HAS_NO_NEXT_ELEMENT);
  end = clock();
  elapsed_time = (end - start) / (double)CLOCKS_PER_SEC;

  query_result_dtor(q_res);
  printf("Matched %d of %d elements of type1 within %lf seconds\n", match1, COUNT, elapsed_time);

  int match2 = 0;

  q.args.as_match.pattern = &el_pattern2;
  start = clock();
  error = process_query(storage, &q, q_res);

  while (error == 0) {
    match2++;
    CHECK(query_result_get_data(q_res, &real) == 0);

    CHECK(is_type_equals(real->element_type, &type2));
    CHECK(!real->attributes[0].value.as_bool);

    error = query_result_next(q_res);
    element_dtor(real);
    free(real);
  }
  CHECK(error == ERROR_HAS_NO_NEXT_ELEMENT);
  end = clock();
  elapsed_time = (end - start) / (double)CLOCKS_PER_SEC;

  query_result_dtor(q_res);
  printf("Matched %d of %d elements of type2 within %lf seconds\n", match2, COUNT, elapsed_time);

  char* updated_string = "UPDATED!UPDATED!UPDATED!";
  q.type = QUERY_TYPE_SET;
  struct attribute_and_id set_attrs1[2] = {
      {.attribute_id = 0, .attribute = {.type = ATTR_TYPE_BOOL, .value.as_bool = true}},
      {.attribute_id = 4, .attribute = {.type = ATTR_TYPE_STRING, .value.as_string = updated_string}}};
  q.args.as_set = (struct set_args){.pattern = &el_pattern1, .attributes_count = 2, .attributes = set_attrs1};

  start = clock();
  CHECK(process_query(storage, &q, NULL) == 0);
  end = clock();
  elapsed_time = (end - start) / (double)CLOCKS_PER_SEC;

  printf("Updated %d of %d elements of type1 within %lf seconds\n", match1, COUNT, elapsed_time);

  q.type = QUERY_TYPE_SET;
  struct attribute_and_id set_attrs2[1] = {
      {.attribute_id = 1, .attribute = {.type = ATTR_TYPE_INT32, .value.as_int32 = -17}}};
  q.args.as_set = (struct set_args){.pattern = &el_pattern2, .attributes_count = 1, .attributes = set_attrs2};

  start = clock();
  CHECK(process_query(storage, &q, NULL) == 0);
  end = clock();
  elapsed_time = (end - start) / (double)CLOCKS_PER_SEC;

  printf("Updated %d of %d elements of type2 within %lf seconds\n", match2, COUNT, elapsed_time);

  element_storage_dtor(storage);
  free(storage);

  CHECK(element_storage_alloc(&storage));
  CHECK(element_storage_read(storage, "test.txt", 4096));

  struct element_pattern any_element = {.element_type = &type1};
  q.type = QUERY_TYPE_MATCH;
  q.args.as_match.pattern = &any_element;

  error = process_query(storage, &q, q_res);

  start = clock();
  int match1_updated = 0;
  while (error == 0) {
    CHECK(query_result_get_data(q_res, &real) == 0);

    CHECK(is_type_equals(real->element_type, &type1));
    if (real->attributes[3].value.as_int64 == INT64_MAX) {
      match1_updated++;
      CHECK(real->attributes[0].value.as_bool);
      CHECK(strcmp(real->attributes[4].value.as_string.data, updated_string) == 0);
    } else {
      CHECK(!real->attributes[0].value.as_bool);
      CHECK(strcmp(real->attributes[4].value.as_string.data, init_string) == 0);
    }

    error = query_result_next(q_res);
    element_dtor(real);
    free(real);
  }
  end = clock();
  elapsed_time = (end - start) / (double)CLOCKS_PER_SEC;

  printf("Matched %d of %d elements of type1 within %lf seconds\n", match1_updated, COUNT, elapsed_time);
  query_result_dtor(q_res);

  CHECK(error == ERROR_HAS_NO_NEXT_ELEMENT);
  CHECK(match1_updated == match1);

  any_element.element_type = &type2;

  error = process_query(storage, &q, q_res);

  int match2_updated = 0;
  start = clock();
  while (error == 0) {
    CHECK(query_result_get_data(q_res, &real) == 0);

    CHECK(is_type_equals(real->element_type, &type2));
    if (!real->attributes[0].value.as_bool) {
      match2_updated++;
      CHECK(real->attributes[1].value.as_int32 == -17);
    } else {
      CHECK(real->attributes[1].value.as_int32 == 0);
    }

    error = query_result_next(q_res);
    element_dtor(real);
    free(real);
  }
  end = clock();
  elapsed_time = (end - start) / (double)CLOCKS_PER_SEC;

  printf("Matched %d of %d elements of type2 within %lf seconds\n", match2_updated, COUNT, elapsed_time);
  query_result_dtor(q_res);

  CHECK(error == ERROR_HAS_NO_NEXT_ELEMENT);
  CHECK(match2_updated == match2);

  free(q_res);
  element_storage_dtor(storage);
  free(storage);
}

void tear_down() {
  remove("test.txt");
}

int main() {
#ifndef DISABLED
  RUN_TEST(create_match_update_close_match);
  tear_down();
#else
  printf("Test disabled\n");
#endif /* DISABLED */
  return 0;
}