#include "element_check.h"

#include <inttypes.h>

bool is_type_equals(struct element_type* lhs, struct element_type* rhs) {
  if (lhs->attribute_count != rhs->attribute_count) {
    return false;
  }
  if (lhs->type_id != rhs->type_id) {
    return false;
  }
  if (strcmp(lhs->type_name, rhs->type_name) != 0) {
    return false;
  }
  if (memcmp(lhs->attribute_types, rhs->attribute_types, sizeof(enum ATTRIBUTE_TYPE) * lhs->attribute_count) != 0) {
    return false;
  }
  return true;
}

bool is_attrs_equals(struct attribute* lhs, struct attribute* rhs, attribute_count_t cnt) {
  for (attribute_count_t i = 0; i < cnt; i++) {
    if (lhs[i].type != rhs[i].type) {
      return false;
    }
    switch (lhs[i].type) {
      case ATTR_TYPE_STRING:
        if (strcmp(lhs[i].value.as_string.data, rhs[i].value.as_string.data) != 0) {
          return false;
        }
        break;
      case ATTR_TYPE_BOOL:
        if (lhs[i].value.as_bool != rhs[i].value.as_bool) {
          return false;
        }
        break;
      case ATTR_TYPE_DOUBLE:
        if (lhs[i].value.as_double != rhs[i].value.as_double) {
          return false;
        }
        break;
      case ATTR_TYPE_INT32:
        if (lhs[i].value.as_int32 != rhs[i].value.as_int32) {
          return false;
        }
        break;
      case ATTR_TYPE_INT64:
        if (lhs[i].value.as_int64 != rhs[i].value.as_int64) {
          return false;
        }
        break;
      default:
        return false;
        break;
    }
  }
  return true;
}

void dump_element(struct element* el) {
  if (el->element_type != NULL) {
    printf("Element type of: %s %d\n", el->element_type->type_name, el->element_type->type_id);
  }
  for (attribute_count_t i = 0; i < el->attribute_count; i++) {
    switch (el->attributes[i].type) {
      case ATTR_TYPE_BOOL:
        printf("Field %d: %d\n", i, el->attributes[i].value.as_bool);
        break;
      case ATTR_TYPE_INT32:
        printf("Field %d: %d\n", i, el->attributes[i].value.as_int32);
        break;
      case ATTR_TYPE_INT64:
        printf("Field %d: %" PRId64 "\n", i, el->attributes[i].value.as_int64);
        break;
      case ATTR_TYPE_DOUBLE:
        printf("Field %d: %lf\n", i, el->attributes[i].value.as_double);
        break;
      case ATTR_TYPE_STRING:
        printf("Field %d: %s\n", i, el->attributes[i].value.as_string.data);
        break;
      default:
        printf("Unknown filed %d\n", i);
        break;
    }
  }
}
