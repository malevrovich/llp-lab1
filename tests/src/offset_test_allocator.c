#include "offset_test_allocator_interface.h"

#include "offset_fixed_base_allocator.h"

#include <stdlib.h>

#include "common_types.h"

struct offset_test_allocator {
  struct offset_fixed_base_allocator base_alloc;
};

bool offset_test_allocator_alloc(struct offset_test_allocator** ptr) {
  *ptr = malloc(sizeof(struct offset_test_allocator));
  return (*ptr != NULL);
}

mem_offset_t offset_test_allocator_allocate_f(struct offset_fixed_base_allocator* alloc) {
  return (mem_offset_t){.data_block = malloc(alloc->alloc_size.value) - (void*){0}};
}

void offset_test_allocator_free_f(struct offset_fixed_base_allocator* alloc, mem_offset_t target) {
  free((void*)target.data_block);
}

void offset_test_allocator_ctor(struct offset_test_allocator* alloc, mem_size_t alloc_size) {
  alloc->base_alloc.alloc_size = alloc_size;
  alloc->base_alloc.allocate_f = &offset_test_allocator_allocate_f;
  alloc->base_alloc.free_f = &offset_test_allocator_free_f;
}
void offset_test_allocator_dtor(struct offset_test_allocator* alloc) {}