#include "offset_test_memory_manager_interface.h"

#include <stdlib.h>
#include <string.h>

#include "common_types.h"
#include "offset_memory_manager.h"
#include "test_utils.h"

struct offset_test_memory_manager {
  struct offset_memory_manager base_manager;

  void* base;
  mem_size_t allocated;
};

bool offset_test_memory_manager_alloc(struct offset_test_memory_manager** ptr) {
  *ptr = malloc(sizeof(struct offset_test_memory_manager));
  return (*ptr != NULL);
}

mem_offset_t offset_test_memory_manager_extend_f(struct offset_memory_manager* mem_manager, mem_size_t size) {
  struct offset_test_memory_manager* self = (struct offset_test_memory_manager*)mem_manager;

  CHECK(self->base_manager.size.value + size.value <= self->allocated.value);

  mem_offset_t result = {.data_block = (uint64_t)((char*)self->base + self->base_manager.size.value), .value = 0};

  return result;
}

void offset_test_memory_manager_cut_f(struct offset_memory_manager* mem_manager, mem_size_t size) {
  struct offset_test_memory_manager* self = (struct offset_test_memory_manager*)mem_manager;

  CHECK(self->base_manager.size.value - size.value >= 0);
}

void offset_test_memory_manager_read_f(struct offset_memory_manager* manager, mem_offset_t offset, mem_size_t buf_size,
                                       void* buffer) {
  struct offset_test_memory_manager* self = (struct offset_test_memory_manager*)manager;
  void* target = (void*)(offset.data_block + offset.value);
  memcpy(buffer, target, buf_size.value);
}
void offset_test_memory_manager_write_f(struct offset_memory_manager* manager, mem_offset_t offset, mem_size_t buf_size,
                                        const void* buffer) {
  struct offset_test_memory_manager* self = (struct offset_test_memory_manager*)manager;
  void* target = (void*)(offset.data_block + offset.value);
  memcpy(target, buffer, buf_size.value);
}
void offset_test_memory_manager_ctor(struct offset_test_memory_manager* mem_manager, mem_size_t size) {
  offset_memory_manager_ctor(&mem_manager->base_manager, (mem_size_t){0});
  mem_manager->base_manager.extend_f = &offset_test_memory_manager_extend_f;
  mem_manager->base_manager.read_f = &offset_test_memory_manager_read_f;
  mem_manager->base_manager.write_f = &offset_test_memory_manager_write_f;
  mem_manager->base_manager.cut_f = &offset_test_memory_manager_cut_f;

  mem_manager->allocated = size;
  mem_manager->base = malloc(size.value);
}
void offset_test_memory_manager_dtor(struct offset_test_memory_manager* mem_manager) {
  free(mem_manager->base);
}
