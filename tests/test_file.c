#include "offset_unix_file_manager_interface.h"
#include "offset_windows_file_manager_interface.h"
#include "test_utils.h"

#include <stdio.h>

#include <errno.h>
#include <string.h>

#define PAGE_SIZE 4096

#if !defined(_WIN32) && !defined(__unix__)
#define DISABLED
#else
TEST(simple) {
#ifdef _WIN32
  struct offset_windows_file_manager* win_mem_manager;

  CHECK(offset_windows_file_manager_alloc(&win_mem_manager));

  struct offset_memory_manager* mem_manager = (struct offset_memory_manager*)win_mem_manager;

  CHECK(offset_windows_file_manager_ctor(win_mem_manager, "test.txt", (mem_size_t){.value = PAGE_SIZE}));
#endif /* _WIN32 */
#ifdef __unix__
  struct offset_unix_file_manager* unix_mem_manager;

  CHECK(offset_unix_file_manager_alloc(&unix_mem_manager));

  struct offset_memory_manager* mem_manager = (struct offset_memory_manager*)unix_mem_manager;

  CHECK(offset_unix_file_manager_ctor(unix_mem_manager, "test.txt", (mem_size_t){.value = PAGE_SIZE}));
#endif /* __unix__ */

  mem_size_t data_size = {.value = 25 << 20};

  mem_offset_t elem[3];
  char* data[3];
  for (int i = 0; i < 3; i++) {
    data[i] = malloc(data_size.value);
  }

  for (int i = 0; i < 3; i++) {
    elem[i] = offset_memory_manager_extend(mem_manager, (mem_size_t){.value = data_size.value});
    CHECK(!IS_EQUAL_OFFSET(elem[i], MEM_NULL_OFFSET));
  }

  for (int i = 0; i < 3; i++) {
    int seed = i;
    for (int k = 0; k < data_size.value; k++) {
      seed = (1103515245 * seed + 12345) % 26;
      data[i][k] = 'a' + seed;
    }
    offset_memory_manager_write(mem_manager, elem[i], data_size, data[i]);
  }

  for (int i = 0; i < 3; i++) {
    char* real = malloc(data_size.value);
    offset_memory_manager_read(mem_manager, elem[i], data_size, real);
    CHECK(memcmp(real, data[i], data_size.value) == 0);
    free(real);
  }

#ifdef _WIN32
  offset_windows_file_manager_dtor(win_mem_manager);
  CHECK(offset_windows_file_manager_ctor(win_mem_manager, "test.txt", (mem_size_t){.value = PAGE_SIZE}));
#endif /* _WIN32 */
#ifdef __unix__
  offset_unix_file_manager_dtor(unix_mem_manager);
  CHECK(offset_unix_file_manager_ctor(unix_mem_manager, "test.txt", (mem_size_t){.value = PAGE_SIZE}));
#endif

  char* real = malloc(3 * data_size.value);
  offset_memory_manager_read(mem_manager, elem[0], (mem_size_t){.value = 1 * data_size.value}, real);
  for (int i = 0; i < 1; i++) {
    CHECK(memcmp(real + data_size.value * i, data[i], data_size.value) == 0);
  }
  free(real);

  offset_memory_manager_cut(mem_manager, data_size);
  for (int i = 0; i < 2; i++) {
    char* real = malloc(data_size.value);
    offset_memory_manager_read(mem_manager, elem[i], data_size, real);
    CHECK(memcmp(real, data[i], data_size.value) == 0);
    free(real);
  }

  offset_memory_manager_cut(mem_manager, data_size);
  for (int i = 0; i < 1; i++) {
    char* real = malloc(data_size.value);
    offset_memory_manager_read(mem_manager, elem[i], data_size, real);
    CHECK(memcmp(real, data[i], data_size.value) == 0);
    free(real);
  }

  offset_memory_manager_cut(mem_manager, data_size);

  for (int i = 0; i < 3; i++) {
    free(data[i]);
  }

#ifdef _WIN32
  offset_windows_file_manager_dtor(win_mem_manager);
  free(win_mem_manager);
#endif /* _WIN32 */
#ifdef __unix__
  offset_unix_file_manager_dtor(unix_mem_manager);
  free(unix_mem_manager);
#endif /* __unix__ */
}

void tear_down() {
  remove("test.txt");
}
#endif /* !defined(_WIN32) && !define(__unix__) */

int main() {
#ifndef DISABLED
  RUN_TEST(simple);
  tear_down();
#else
  printf("Test disabled\n");
#endif /* DISABLED */
  return 0;
}
