#include "test_utils.h"

#include "offset_element_allocator_interface.h"
#include "offset_element_io_interface.h"
#include "offset_list_extended_interface.h"
#include "offset_page_allocator_interface.h"
#include "offset_test_allocator_interface.h"
#include "offset_test_memory_manager_interface.h"

#include "page.h"

#define CHECK_SIZE_IN_PAGES(test_mem_manager, alloc_size, page_count)                            \
  CHECK(offset_memory_manager_get_size((struct offset_memory_manager*)test_mem_manager).value == \
        offset_page_allocator_get_page_size(alloc_size).value * page_count)

TEST(page_allocator) {
  struct offset_page_allocator* page_alloc;
  offset_page_allocator_alloc(&page_alloc);

  struct offset_fixed_base_allocator* header_alloc;
  offset_test_allocator_alloc((struct offset_test_allocator**)&header_alloc);
  offset_test_allocator_ctor((struct offset_test_allocator*)header_alloc, offset_page_allocator_get_data_size());

  mem_offset_t page_alloc_offset =
      offset_fixed_base_allocator_allocate((struct offset_fixed_base_allocator*)header_alloc);

  mem_size_t alloc_size = (mem_size_t){128 * sizeof(char)};

  struct offset_memory_manager* mem_manager;
  offset_test_memory_manager_alloc((struct offset_test_memory_manager**)&mem_manager);

  struct offset_test_memory_manager* test_mem_manager = (struct offset_test_memory_manager*)mem_manager;
  offset_test_memory_manager_ctor((struct offset_test_memory_manager*)mem_manager,
                                  (mem_size_t){offset_page_allocator_get_page_size(alloc_size).value * 5});

  offset_page_allocator_ctor(page_alloc, page_alloc_offset, mem_manager, alloc_size);

  struct offset_fixed_base_allocator* as_base = (struct offset_fixed_base_allocator*)page_alloc;

  mem_offset_t pages[5];
  for (int i = 0; i < 5; i++) {
    pages[i] = offset_fixed_base_allocator_allocate(as_base);
  }

  CHECK_SIZE_IN_PAGES(test_mem_manager, alloc_size, 5);

  offset_fixed_base_allocator_free(as_base, pages[0]);
  offset_fixed_base_allocator_free(as_base, pages[4]);

  CHECK_SIZE_IN_PAGES(test_mem_manager, alloc_size, 4);

  pages[0] = offset_fixed_base_allocator_allocate(as_base);

  CHECK_SIZE_IN_PAGES(test_mem_manager, alloc_size, 4);

  pages[4] = offset_fixed_base_allocator_allocate(as_base);

  CHECK_SIZE_IN_PAGES(test_mem_manager, alloc_size, 5);

  offset_fixed_base_allocator_free(as_base, pages[3]);
  offset_fixed_base_allocator_free(as_base, pages[2]);

  CHECK_SIZE_IN_PAGES(test_mem_manager, alloc_size, 5);

  offset_fixed_base_allocator_free(as_base, pages[4]);

  CHECK_SIZE_IN_PAGES(test_mem_manager, alloc_size, 2);

  offset_fixed_base_allocator_free(as_base, pages[0]);
  offset_fixed_base_allocator_free(as_base, pages[1]);

  CHECK_SIZE_IN_PAGES(test_mem_manager, alloc_size, 0);

  for (int i = 0; i < 5; i++) {
    pages[i] = offset_fixed_base_allocator_allocate(as_base);
  }

  CHECK_SIZE_IN_PAGES(test_mem_manager, alloc_size, 5);

  char data[128 * sizeof(char)];

  offset_memory_manager_write((struct offset_memory_manager*)test_mem_manager, pages[4],
                              (mem_size_t){128 * sizeof(char)}, data);

  offset_test_memory_manager_dtor(test_mem_manager);
}

TEST(element_allocator_eq) {
  mem_size_t data_size = (mem_size_t){128 * sizeof(char)};
  mem_size_t page_alloc_size = (mem_size_t){sizeof(struct elements_page_header) + data_size.value};
  int page_count = 2;

  struct offset_page_allocator* page_alloc;
  offset_page_allocator_alloc(&page_alloc);

  struct offset_fixed_base_allocator* page_header_alloc;
  offset_test_allocator_alloc((struct offset_test_allocator**)&page_header_alloc);
  offset_test_allocator_ctor((struct offset_test_allocator*)page_header_alloc, offset_page_allocator_get_data_size());

  mem_offset_t page_alloc_offset =
      offset_fixed_base_allocator_allocate((struct offset_fixed_base_allocator*)page_header_alloc);

  struct offset_memory_manager* mem_manager;
  offset_test_memory_manager_alloc((struct offset_test_memory_manager**)&mem_manager);

  struct offset_test_memory_manager* test_mem_manager = (struct offset_test_memory_manager*)mem_manager;
  offset_test_memory_manager_ctor(
      (struct offset_test_memory_manager*)mem_manager,
      (mem_size_t){offset_page_allocator_get_page_size(page_alloc_size).value * page_count});

  offset_page_allocator_ctor(page_alloc, page_alloc_offset, mem_manager, page_alloc_size);

  struct offset_element_allocator* element_allocator;
  offset_element_allocator_alloc(&element_allocator);

  struct offset_fixed_base_allocator* element_header_allocator;
  offset_test_allocator_alloc((struct offset_test_allocator**)&element_header_allocator);

  offset_test_allocator_ctor((struct offset_test_allocator*)element_header_allocator,
                             offset_element_allocator_get_data_size());

  mem_offset_t element_allocator_offset = offset_fixed_base_allocator_allocate(element_header_allocator);

  offset_element_allocator_ctor(element_allocator, element_allocator_offset, page_alloc, mem_manager);

  mem_offset_t elem_1 = offset_element_allocator_allocate(element_allocator, data_size);

  CHECK_SIZE_IN_PAGES(test_mem_manager, page_alloc_size, 1);
  mem_offset_t elem_2 = offset_element_allocator_allocate(element_allocator, data_size);
  CHECK_SIZE_IN_PAGES(test_mem_manager, page_alloc_size, 2);

  offset_element_allocator_free(element_allocator, elem_1, data_size);

  CHECK_SIZE_IN_PAGES(test_mem_manager, page_alloc_size, 2);

  elem_1 = offset_element_allocator_allocate(element_allocator, data_size);

  CHECK_SIZE_IN_PAGES(test_mem_manager, page_alloc_size, 2);

  offset_element_allocator_free(element_allocator, elem_1, data_size);

  CHECK_SIZE_IN_PAGES(test_mem_manager, page_alloc_size, 2);

  offset_element_allocator_free(element_allocator, elem_2, data_size);

  CHECK_SIZE_IN_PAGES(test_mem_manager, page_alloc_size, 0);

  offset_test_memory_manager_dtor(test_mem_manager);
}

TEST(element_allocator_less) {
  mem_size_t data_size = (mem_size_t){128 * sizeof(char)};
  mem_size_t page_alloc_size = (mem_size_t){sizeof(struct elements_page_header) + data_size.value * 2};
  int page_count = 2;

  struct offset_page_allocator* page_alloc;
  offset_page_allocator_alloc(&page_alloc);

  struct offset_fixed_base_allocator* page_header_alloc;
  offset_test_allocator_alloc((struct offset_test_allocator**)&page_header_alloc);
  offset_test_allocator_ctor((struct offset_test_allocator*)page_header_alloc, offset_page_allocator_get_data_size());

  mem_offset_t page_alloc_offset =
      offset_fixed_base_allocator_allocate((struct offset_fixed_base_allocator*)page_header_alloc);

  struct offset_memory_manager* mem_manager;
  offset_test_memory_manager_alloc((struct offset_test_memory_manager**)&mem_manager);

  struct offset_test_memory_manager* test_mem_manager = (struct offset_test_memory_manager*)mem_manager;
  offset_test_memory_manager_ctor(
      (struct offset_test_memory_manager*)mem_manager,
      (mem_size_t){offset_page_allocator_get_page_size(page_alloc_size).value * page_count});

  offset_page_allocator_ctor(page_alloc, page_alloc_offset, mem_manager, page_alloc_size);

  struct offset_element_allocator* element_allocator;
  offset_element_allocator_alloc(&element_allocator);

  struct offset_fixed_base_allocator* element_header_allocator;
  offset_test_allocator_alloc((struct offset_test_allocator**)&element_header_allocator);

  offset_test_allocator_ctor((struct offset_test_allocator*)element_header_allocator,
                             offset_element_allocator_get_data_size());

  mem_offset_t element_allocator_offset = offset_fixed_base_allocator_allocate(element_header_allocator);

  offset_element_allocator_ctor(element_allocator, element_allocator_offset, page_alloc, mem_manager);

  mem_offset_t elem[4];
  elem[0] = offset_element_allocator_allocate(element_allocator, data_size);
  CHECK_SIZE_IN_PAGES(test_mem_manager, page_alloc_size, 1);
  elem[1] = offset_element_allocator_allocate(element_allocator, data_size);
  CHECK_SIZE_IN_PAGES(test_mem_manager, page_alloc_size, 1);
  elem[2] = offset_element_allocator_allocate(element_allocator, data_size);
  CHECK_SIZE_IN_PAGES(test_mem_manager, page_alloc_size, 2);
  elem[3] = offset_element_allocator_allocate(element_allocator, data_size);
  CHECK_SIZE_IN_PAGES(test_mem_manager, page_alloc_size, 2);

  char data[4][data_size.value];

  for (int i = 0; i < 4; i++) {
    for (int k = 0; k < data_size.value; k++) {
      data[i][k] = 'a' + i;
    }
    offset_memory_manager_write((struct offset_memory_manager*)test_mem_manager, elem[i], data_size, data[i]);
  }

  for (int i = 0; i < 4; i++) {
    char real[data_size.value];
    offset_memory_manager_read((struct offset_memory_manager*)test_mem_manager, elem[i], data_size, real);
    CHECK(memcmp(real, data[i], data_size.value) == 0);
  }

  offset_element_allocator_free(element_allocator, elem[1], data_size);
  offset_element_allocator_free(element_allocator, elem[3], data_size);
  CHECK_SIZE_IN_PAGES(test_mem_manager, page_alloc_size, 2);
  offset_element_allocator_free(element_allocator, elem[2], data_size);
  CHECK_SIZE_IN_PAGES(test_mem_manager, page_alloc_size, 1);
  offset_element_allocator_free(element_allocator, elem[0], data_size);
  CHECK_SIZE_IN_PAGES(test_mem_manager, page_alloc_size, 0);

  offset_test_memory_manager_dtor(test_mem_manager);
}

TEST(element_allocator_greater) {
  mem_size_t data_size = (mem_size_t){128 * sizeof(char)};
  mem_size_t page_alloc_size = (mem_size_t){sizeof(struct elements_page_header) + 64 * sizeof(char)};
  int page_count = 4;

  struct offset_page_allocator* page_alloc;
  offset_page_allocator_alloc(&page_alloc);

  struct offset_fixed_base_allocator* page_header_alloc;
  offset_test_allocator_alloc((struct offset_test_allocator**)&page_header_alloc);
  offset_test_allocator_ctor((struct offset_test_allocator*)page_header_alloc, offset_page_allocator_get_data_size());

  mem_offset_t page_alloc_offset =
      offset_fixed_base_allocator_allocate((struct offset_fixed_base_allocator*)page_header_alloc);

  struct offset_memory_manager* mem_manager;
  offset_test_memory_manager_alloc((struct offset_test_memory_manager**)&mem_manager);

  struct offset_test_memory_manager* test_mem_manager = (struct offset_test_memory_manager*)mem_manager;
  offset_test_memory_manager_ctor(
      (struct offset_test_memory_manager*)mem_manager,
      (mem_size_t){offset_page_allocator_get_page_size(page_alloc_size).value * page_count});

  offset_page_allocator_ctor(page_alloc, page_alloc_offset, mem_manager, page_alloc_size);

  struct offset_element_allocator* element_allocator;
  offset_element_allocator_alloc(&element_allocator);

  struct offset_fixed_base_allocator* element_header_allocator;
  offset_test_allocator_alloc((struct offset_test_allocator**)&element_header_allocator);

  offset_test_allocator_ctor((struct offset_test_allocator*)element_header_allocator,
                             offset_element_allocator_get_data_size());

  mem_offset_t element_allocator_offset = offset_fixed_base_allocator_allocate(element_header_allocator);

  offset_element_allocator_ctor(element_allocator, element_allocator_offset, page_alloc, mem_manager);

  mem_offset_t elem_1 = offset_element_allocator_allocate(element_allocator, data_size);
  CHECK_SIZE_IN_PAGES(test_mem_manager, page_alloc_size, 2);
  mem_offset_t elem_2 = offset_element_allocator_allocate(element_allocator, data_size);
  CHECK_SIZE_IN_PAGES(test_mem_manager, page_alloc_size, 4);

  offset_element_allocator_free(element_allocator, elem_2, data_size);
  CHECK_SIZE_IN_PAGES(test_mem_manager, page_alloc_size, 2);

  elem_2 = offset_element_allocator_allocate(element_allocator, data_size);
  CHECK_SIZE_IN_PAGES(test_mem_manager, page_alloc_size, 4);

  offset_element_allocator_free(element_allocator, elem_1, data_size);
  CHECK_SIZE_IN_PAGES(test_mem_manager, page_alloc_size, 4);

  offset_element_allocator_free(element_allocator, elem_2, data_size);
  CHECK_SIZE_IN_PAGES(test_mem_manager, page_alloc_size, 0);

  elem_1 = offset_element_allocator_allocate(element_allocator, data_size);
  CHECK_SIZE_IN_PAGES(test_mem_manager, page_alloc_size, 2);
  offset_element_allocator_free(element_allocator, elem_1, data_size);
  CHECK_SIZE_IN_PAGES(test_mem_manager, page_alloc_size, 0);
}

TEST(element_allocator_semi) {
  mem_size_t data_size = (mem_size_t){194 * sizeof(char)};
  mem_size_t page_alloc_size = (mem_size_t){sizeof(struct elements_page_header) + 130 * sizeof(char)};
  int page_count = 5;

  struct offset_page_allocator* page_alloc;
  offset_page_allocator_alloc(&page_alloc);

  struct offset_fixed_base_allocator* page_header_alloc;
  offset_test_allocator_alloc((struct offset_test_allocator**)&page_header_alloc);
  offset_test_allocator_ctor((struct offset_test_allocator*)page_header_alloc, offset_page_allocator_get_data_size());

  mem_offset_t page_alloc_offset =
      offset_fixed_base_allocator_allocate((struct offset_fixed_base_allocator*)page_header_alloc);

  struct offset_memory_manager* mem_manager;
  offset_test_memory_manager_alloc((struct offset_test_memory_manager**)&mem_manager);

  struct offset_test_memory_manager* test_mem_manager = (struct offset_test_memory_manager*)mem_manager;
  offset_test_memory_manager_ctor(
      (struct offset_test_memory_manager*)mem_manager,
      (mem_size_t){offset_page_allocator_get_page_size(page_alloc_size).value * page_count});

  offset_page_allocator_ctor(page_alloc, page_alloc_offset, mem_manager, page_alloc_size);

  struct offset_element_allocator* element_allocator;
  offset_element_allocator_alloc(&element_allocator);

  struct offset_fixed_base_allocator* element_header_allocator;
  offset_test_allocator_alloc((struct offset_test_allocator**)&element_header_allocator);

  offset_test_allocator_ctor((struct offset_test_allocator*)element_header_allocator,
                             offset_element_allocator_get_data_size());

  mem_offset_t element_allocator_offset = offset_fixed_base_allocator_allocate(element_header_allocator);

  offset_element_allocator_ctor(element_allocator, element_allocator_offset, page_alloc, mem_manager);

  mem_offset_t elem_1 = offset_element_allocator_allocate(element_allocator, data_size);
  CHECK_SIZE_IN_PAGES(test_mem_manager, page_alloc_size, 2);
  mem_offset_t elem_2 = offset_element_allocator_allocate(element_allocator, data_size);
  CHECK_SIZE_IN_PAGES(test_mem_manager, page_alloc_size, 3);

  mem_offset_t elem_3 = offset_element_allocator_allocate(element_allocator, data_size);
  CHECK_SIZE_IN_PAGES(test_mem_manager, page_alloc_size, 5);

  offset_element_allocator_free(element_allocator, elem_3, data_size);
  CHECK_SIZE_IN_PAGES(test_mem_manager, page_alloc_size, 3);

  elem_3 = offset_element_allocator_allocate(element_allocator, data_size);
  CHECK_SIZE_IN_PAGES(test_mem_manager, page_alloc_size, 5);

  offset_element_allocator_free(element_allocator, elem_3, data_size);
  CHECK_SIZE_IN_PAGES(test_mem_manager, page_alloc_size, 3);
  offset_element_allocator_free(element_allocator, elem_2, data_size);
  CHECK_SIZE_IN_PAGES(test_mem_manager, page_alloc_size, 2);

  elem_2 = offset_element_allocator_allocate(element_allocator, data_size);
  CHECK_SIZE_IN_PAGES(test_mem_manager, page_alloc_size, 4);

  elem_3 = offset_element_allocator_allocate(element_allocator, data_size);
  CHECK_SIZE_IN_PAGES(test_mem_manager, page_alloc_size, 5);

  offset_element_allocator_free(element_allocator, elem_1, data_size);
  CHECK_SIZE_IN_PAGES(test_mem_manager, page_alloc_size, 5);

  elem_1 = offset_element_allocator_allocate(element_allocator, data_size);
  CHECK_SIZE_IN_PAGES(test_mem_manager, page_alloc_size, 5);

  offset_element_allocator_free(element_allocator, elem_1, data_size);
  CHECK_SIZE_IN_PAGES(test_mem_manager, page_alloc_size, 5);

  offset_element_allocator_free(element_allocator, elem_3, data_size);
  CHECK_SIZE_IN_PAGES(test_mem_manager, page_alloc_size, 4);

  offset_element_allocator_free(element_allocator, elem_2, data_size);
  CHECK_SIZE_IN_PAGES(test_mem_manager, page_alloc_size, 0);

  elem_1 = offset_element_allocator_allocate(element_allocator, data_size);
  CHECK_SIZE_IN_PAGES(test_mem_manager, page_alloc_size, 2);

  elem_2 = offset_element_allocator_allocate(element_allocator, data_size);
  CHECK_SIZE_IN_PAGES(test_mem_manager, page_alloc_size, 3);

  elem_3 = offset_element_allocator_allocate(element_allocator, data_size);
  CHECK_SIZE_IN_PAGES(test_mem_manager, page_alloc_size, 5);

  offset_element_allocator_free(element_allocator, elem_3, data_size);
  CHECK_SIZE_IN_PAGES(test_mem_manager, page_alloc_size, 3);

  offset_element_allocator_free(element_allocator, elem_1, data_size);
  CHECK_SIZE_IN_PAGES(test_mem_manager, page_alloc_size, 3);

  offset_element_allocator_free(element_allocator, elem_2, data_size);
  CHECK_SIZE_IN_PAGES(test_mem_manager, page_alloc_size, 0);

  offset_test_memory_manager_dtor(test_mem_manager);
}

TEST(long_io) {
  mem_size_t data_size = (mem_size_t){194 * sizeof(char)};
  mem_size_t page_alloc_size = (mem_size_t){sizeof(struct elements_page_header) + 130 * sizeof(char)};
  int page_count = 5;

  struct offset_page_allocator* page_alloc;
  offset_page_allocator_alloc(&page_alloc);

  struct offset_fixed_base_allocator* page_header_alloc;
  offset_test_allocator_alloc((struct offset_test_allocator**)&page_header_alloc);
  offset_test_allocator_ctor((struct offset_test_allocator*)page_header_alloc, offset_page_allocator_get_data_size());

  mem_offset_t page_alloc_offset =
      offset_fixed_base_allocator_allocate((struct offset_fixed_base_allocator*)page_header_alloc);

  struct offset_memory_manager* mem_manager;
  offset_test_memory_manager_alloc((struct offset_test_memory_manager**)&mem_manager);

  struct offset_test_memory_manager* test_mem_manager = (struct offset_test_memory_manager*)mem_manager;
  offset_test_memory_manager_ctor(
      (struct offset_test_memory_manager*)mem_manager,
      (mem_size_t){offset_page_allocator_get_page_size(page_alloc_size).value * page_count});

  offset_page_allocator_ctor(page_alloc, page_alloc_offset, mem_manager, page_alloc_size);

  struct offset_element_allocator* element_allocator;
  offset_element_allocator_alloc(&element_allocator);

  struct offset_fixed_base_allocator* element_header_allocator;
  offset_test_allocator_alloc((struct offset_test_allocator**)&element_header_allocator);

  offset_test_allocator_ctor((struct offset_test_allocator*)element_header_allocator,
                             offset_element_allocator_get_data_size());

  mem_offset_t element_allocator_offset = offset_fixed_base_allocator_allocate(element_header_allocator);

  offset_element_allocator_ctor(element_allocator, element_allocator_offset, page_alloc, mem_manager);

  struct offset_memory_manager* element_mem_manager;
  offset_element_io_alloc((struct offset_element_io**)&element_mem_manager);

  offset_element_io_ctor((struct offset_element_io*)element_mem_manager,
                         (struct offset_memory_manager*)test_mem_manager);

  mem_offset_t elem[3];

  elem[0] = offset_element_allocator_allocate(element_allocator, data_size);
  CHECK_SIZE_IN_PAGES(test_mem_manager, page_alloc_size, 2);
  elem[1] = offset_element_allocator_allocate(element_allocator, data_size);
  CHECK_SIZE_IN_PAGES(test_mem_manager, page_alloc_size, 3);
  elem[2] = offset_element_allocator_allocate(element_allocator, data_size);
  CHECK_SIZE_IN_PAGES(test_mem_manager, page_alloc_size, 5);

  char data[3][data_size.value];

  for (int i = 0; i < 3; i++) {
    for (int k = 0; k < data_size.value; k++) {
      data[i][k] = 'a' + i;
    }
    offset_memory_manager_write(element_mem_manager, elem[i], data_size, data[i]);
  }

  for (int i = 0; i < 3; i++) {
    char real[data_size.value];
    offset_memory_manager_read(element_mem_manager, elem[i], data_size, real);
    CHECK(memcmp(real, data[i], data_size.value) == 0);
  }

  offset_element_allocator_free(element_allocator, elem[2], data_size);
  CHECK_SIZE_IN_PAGES(test_mem_manager, page_alloc_size, 3);
  offset_element_allocator_free(element_allocator, elem[1], data_size);
  CHECK_SIZE_IN_PAGES(test_mem_manager, page_alloc_size, 2);
  offset_element_allocator_free(element_allocator, elem[0], data_size);
  CHECK_SIZE_IN_PAGES(test_mem_manager, page_alloc_size, 0);

  // [0 0 0 0][0 0|_ _][_ _ _ _][_ _ _ _][_ _ _ _]
  elem[0] = offset_element_allocator_allocate(element_allocator, data_size);
  CHECK_SIZE_IN_PAGES(test_mem_manager, page_alloc_size, 2);
  // [0 0 0 0][0 0 1 1][1 1 1 1|][_ _ _ _][_ _ _ _]
  elem[1] = offset_element_allocator_allocate(element_allocator, data_size);
  CHECK_SIZE_IN_PAGES(test_mem_manager, page_alloc_size, 3);
  // [_ _ _ _][_ _ 1 1][1 1 1 1|][_ _ _ _][_ _ _ _]
  offset_element_allocator_free(element_allocator, elem[0], data_size);
  CHECK_SIZE_IN_PAGES(test_mem_manager, page_alloc_size, 3);
  // [0 0 0 0][_ _ 1 1][1 1 1 1][0 0|_ _][_ _ _ _]
  elem[0] = offset_element_allocator_allocate(element_allocator, data_size);
  CHECK_SIZE_IN_PAGES(test_mem_manager, page_alloc_size, 4);
  // [0 0 0 0][_ _ 1 1][1 1 1 1][0 0 2 2][2 2 2 2|]
  elem[2] = offset_element_allocator_allocate(element_allocator, data_size);
  CHECK_SIZE_IN_PAGES(test_mem_manager, page_alloc_size, 5);
  // [0 0 0 0][_ _ _ _][_ _ _ _][0 0 2 2][2 2 2 2|]
  offset_element_allocator_free(element_allocator, elem[1], data_size);
  CHECK_SIZE_IN_PAGES(test_mem_manager, page_alloc_size, 5);
  // [0 0 0 0][1 1|_ _][1 1 1 1][0 0 2 2][2 2 2 2|]
  elem[1] = offset_element_allocator_allocate(element_allocator, data_size);
  CHECK_SIZE_IN_PAGES(test_mem_manager, page_alloc_size, 5);

  for (int i = 0; i < 3; i++) {
    for (int k = 0; k < data_size.value; k++) {
      data[i][k] = 't' + i;
    }
    offset_memory_manager_write(element_mem_manager, elem[i], data_size, data[i]);
  }

  for (int i = 0; i < 3; i++) {
    char real[data_size.value];
    offset_memory_manager_read(element_mem_manager, elem[i], data_size, real);
    CHECK(memcmp(real, data[i], data_size.value) == 0);
  }

  for (int i = 0; i < 3; i++) {
    for (int k = 0; k < data_size.value; k++) {
      data[i][k] = '0' + i;
    }
    for (int k = 0; k < data_size.value; k++) {
      offset_memory_manager_write(element_mem_manager, ADD_TO_OFFSET(elem[i], k), (mem_size_t){sizeof(char)},
                                  (data[i] + k));
    }
  }

  for (int i = 0; i < 3; i++) {
    for (int k = 0; k < data_size.value; k++) {
      char real = 'a';
      offset_memory_manager_read(element_mem_manager, ADD_TO_OFFSET(elem[i], k), (mem_size_t){sizeof(char)}, &real);
      CHECK(real == data[i][k]);
    }
  }
}

int main() {
#ifndef DISABLED
  RUN_TEST(page_allocator);
  RUN_TEST(element_allocator_eq);
  RUN_TEST(element_allocator_less);
  RUN_TEST(element_allocator_greater);
  RUN_TEST(element_allocator_semi);
  RUN_TEST(long_io);
#else
  printf("Test disabled\n");
#endif /* DISABLED */
  return 0;
}