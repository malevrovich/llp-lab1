#include "test_utils.h"

#include "element_check.h"
#include "query.h"
#include "query_processor.h"

#include <time.h>

#define COUNT 1000000

void create_elements(struct element_storage* storage, int count) {
  enum ATTRIBUTE_TYPE attrs_type1[3] = {ATTR_TYPE_STRING, ATTR_TYPE_BOOL, ATTR_TYPE_DOUBLE};
  enum ATTRIBUTE_TYPE attrs_type2[3] = {ATTR_TYPE_BOOL, ATTR_TYPE_DOUBLE, ATTR_TYPE_STRING};

  struct element_type type1 = {
      .type_id = 0, .type_name = "Type 1", .attribute_count = 3, .attribute_types = attrs_type1};
  struct element_type type2 = {
      .type_id = 234, .type_name = "Second Type!!!", .attribute_count = 3, .attribute_types = attrs_type2};

  struct element e = {.owns_type_ptr = false, .element_type = &type1, .attribute_count = 3};

  struct query q;
  q.type = QUERY_TYPE_CREATE;
  q.args.as_create.element = &e;

  struct attribute attrs1[3] = {{.type = ATTR_TYPE_STRING, .value = "abcdefg"},
                                {.type = ATTR_TYPE_BOOL, .value.as_bool = true},
                                {.type = ATTR_TYPE_DOUBLE, .value.as_double = 0}};

  q.args.as_create.element->attributes = attrs1;

  clock_t start = clock();
  for (int i = 0; i < count; i++) {
    CHECK(process_query(storage, &q, NULL) == 0);
    attrs1[1].value.as_bool = !attrs1[1].value.as_bool;
    attrs1[2].value.as_double += 0.5;
  }
  clock_t end = clock();
  double elapsed_time = (end - start) / (double)CLOCKS_PER_SEC;
  printf("Created %d elements within %lf seconds\n", count, elapsed_time);
}

int delete_some(struct element_storage* storage, int count) {
  enum ATTRIBUTE_TYPE attrs_type1[3] = {ATTR_TYPE_STRING, ATTR_TYPE_BOOL, ATTR_TYPE_DOUBLE};
  enum ATTRIBUTE_TYPE attrs_type2[3] = {ATTR_TYPE_BOOL, ATTR_TYPE_DOUBLE, ATTR_TYPE_STRING};

  struct element_type type1 = {
      .type_id = 0, .type_name = "Type 1", .attribute_count = 3, .attribute_types = attrs_type1};
  struct element_type type2 = {
      .type_id = 234, .type_name = "Second Type!!!", .attribute_count = 3, .attribute_types = attrs_type2};

  struct attribute_pattern simple_attr_pattern_lhs = {
      .node_type = ATTRIBUTE_PATTERN_COND,
      .args.as_cond = {.cond = ATTRIBUTE_PATTERN_COND_LESS,
                       .attr_id = 2,
                       .attr = {.type = ATTR_TYPE_DOUBLE, .value.as_double = (count / 4) * 0.5}}};
  struct attribute_pattern simple_attr_pattern_rhs = {
      .node_type = ATTRIBUTE_PATTERN_COND,
      .args.as_cond = {.cond = ATTRIBUTE_PATTERN_COND_GREATER,
                       .attr_id = 2,
                       .attr = {.type = ATTR_TYPE_DOUBLE, .value.as_double = (3 * count / 4) * 0.5}}};

  struct attribute_pattern simple_attr_pattern = {
      .node_type = ATTRIBUTE_PATTERN_BIN_OP,
      .args.as_bin = {
          .op_type = ATTRIBUTE_PATTERN_OR, .lhs = &simple_attr_pattern_lhs, .rhs = &simple_attr_pattern_rhs}};

  struct element_pattern simple_el_pattern = {.element_type = &type1, .attr_pattern = &simple_attr_pattern};

  struct query q;
  q.type = QUERY_TYPE_MATCH;
  q.args.as_match.pattern = &simple_el_pattern;

  struct query_result* res;
  CHECK(query_result_alloc(&res));

  clock_t start = clock();

  error_code error = process_query(storage, &q, res);
  CHECK(error == 0);

  struct element* real;
  int deleted_count = 0;
  while (error == 0) {
    deleted_count++;
    CHECK(query_result_get_data(res, &real) == 0);

    // dump_element(real);
    CHECK(real->attributes[2].value.as_double < (count / 4) * 0.5 ||
          real->attributes[2].value.as_double > (3 * count / 4) * 0.5);

    CHECK(is_type_equals(real->element_type, &type1));

    error = query_result_next(res);

    element_dtor(real);
    free(real);
  }
  clock_t end = clock();

  double elapsed_time = (end - start) / (double)CLOCKS_PER_SEC;
  query_result_dtor(res);
  free(res);
  CHECK(error == ERROR_HAS_NO_NEXT_ELEMENT);

  printf("Match %d of %d elements within %lf seconds\n", deleted_count, count, elapsed_time);

  q.type = QUERY_TYPE_DELETE;
  q.args.as_delete.pattern = q.args.as_match.pattern;

  start = clock();
  CHECK(process_query(storage, &q, NULL) == 0);
  end = clock();
  elapsed_time = (end - start) / (double)CLOCKS_PER_SEC;
  printf("Deleted %d of %d elements within %lf seconds\n", deleted_count, count, elapsed_time);

  return deleted_count;
}

void delete_all(struct element_storage* storage) {
  enum ATTRIBUTE_TYPE attrs_type1[3] = {ATTR_TYPE_STRING, ATTR_TYPE_BOOL, ATTR_TYPE_DOUBLE};

  struct element_type type1 = {
      .type_id = 0, .type_name = "Type 1", .attribute_count = 3, .attribute_types = attrs_type1};

  struct element_pattern pattern1 = {.element_type = &type1};

  struct query q = {.type = QUERY_TYPE_DELETE, .args.as_delete.pattern = &pattern1};

  clock_t start = clock();
  CHECK(process_query(storage, &q, NULL) == 0);
  clock_t end = clock();
  double elapsed_time = (end - start) / (double)CLOCKS_PER_SEC;
  printf("Deleted all elements within %lf secodns\n", elapsed_time);
}

TEST(first_create) {
  struct element_storage* storage;

  CHECK(element_storage_alloc(&storage));
  CHECK(element_storage_ctor(storage, "test.txt", 4096));

  enum ATTRIBUTE_TYPE attrs_type1[3] = {ATTR_TYPE_STRING, ATTR_TYPE_BOOL, ATTR_TYPE_DOUBLE};
  enum ATTRIBUTE_TYPE attrs_type2[3] = {ATTR_TYPE_BOOL, ATTR_TYPE_DOUBLE, ATTR_TYPE_STRING};

  struct element_type type1 = {
      .type_id = 0, .type_name = "Type 1", .attribute_count = 3, .attribute_types = attrs_type1};
  struct element_type type2 = {
      .type_id = 234, .type_name = "Second Type!!!", .attribute_count = 3, .attribute_types = attrs_type2};

  CHECK(element_storage_add_element_type(storage, &type1, NULL) == 0);
  CHECK(element_storage_add_element_type(storage, &type2, NULL) == 0);

  element_storage_dtor(storage);
  free(storage);
}

TEST(create) {
  struct element_storage* storage;

  CHECK(element_storage_alloc(&storage));
  CHECK(element_storage_read(storage, "test.txt", 4096));

  for (int i = 0; i < 10; i++) {
    create_elements(storage, COUNT);
  }

  element_storage_dtor(storage);
  free(storage);
}

TEST(create_after_delete) {
  struct element_storage* storage;

  CHECK(element_storage_alloc(&storage));
  CHECK(element_storage_read(storage, "test.txt", 4096));

  for (int i = 1; i <= 10; i++) {
    create_elements(storage, i * COUNT / 10);
    delete_some(storage, i * COUNT / 10);
    create_elements(storage, i * COUNT / 10);
    delete_all(storage);
    printf("\n");
  }

  element_storage_dtor(storage);
  free(storage);
}

TEST(create_after_delete_size) {
  struct element_storage* storage;

  CHECK(element_storage_alloc(&storage));
  CHECK(element_storage_read(storage, "test.txt", 4096));

  create_elements(storage, COUNT);
  printf("Check size and type any symbol:\n");
  getchar();
  int deleted = delete_some(storage, COUNT);
  printf("Check size and type any symbol:\n");
  getchar();
  create_elements(storage, deleted);
  printf("Check size and type any symbol:\n");
  getchar();
  create_elements(storage, deleted);
  printf("Check size and type any symbol:\n");
  getchar();

  element_storage_dtor(storage);
  free(storage);
}

void tear_down() {
  remove("test.txt");
}

int main() {
#ifndef DISABLED
  RUN_TEST(first_create);
  RUN_TEST(create);
  tear_down();
  RUN_TEST(first_create);
  RUN_TEST(create_after_delete);
  tear_down();
  RUN_TEST(first_create);
  RUN_TEST(create_after_delete_size);
  tear_down();
#else
  printf("Test disabled\n");
#endif /* DISABLED */
  return 0;
}