#include "test_utils.h"

#include "element_storage_api.h"
#include "offset_windows_file_manager_interface.h"
#include "schema.h"

#define CHECK_ELEMENT_TYPE(real, expected)                             \
  do {                                                                 \
    CHECK(real.attribute_count == expected.attribute_count);           \
    for (attribute_count_t i = 0; i < expected.attribute_count; i++) { \
      CHECK(real.attribute_types[i] == expected.attribute_types[i]);   \
    }                                                                  \
                                                                       \
    CHECK(real.type_id == expected.type_id);                           \
    CHECK(strcmp(real.type_name, expected.type_name) == 0);            \
  } while (0)

#define CHECK_LINK_TYPE(real, expected)                     \
  do {                                                      \
    CHECK(real.type_id == expected.type_id);                \
    CHECK(strcmp(real.type_name, expected.type_name) == 0); \
  } while (0)

TEST(write) {
  struct element_storage* storage;

  CHECK(element_storage_alloc(&storage));
  CHECK(element_storage_ctor(storage, "test.txt", 4096));

  enum ATTRIBUTE_TYPE attrs_type[3] = {ATTR_TYPE_STRING, ATTR_TYPE_INT64, ATTR_TYPE_INT32};
  struct element_type type1 = {
      .type_id = 0, .type_name = "Type 1", .attribute_count = 3, .attribute_types = attrs_type};

  struct storage_iterator* iter;
  CHECK(storage_iterator_alloc(&iter));

  CHECK(element_storage_get_element_type_iter(storage, iter) == ERROR_LIST_IS_EMPTY);

  CHECK(element_storage_add_element_type(storage, &type1, NULL) == 0);

  CHECK(element_storage_get_element_type_iter(storage, iter) == 0);

  struct element_type real;
  CHECK(element_storage_get_element_type(storage, iter, &real) == 0);

  CHECK_ELEMENT_TYPE(real, type1);

  element_type_dtor(&real);

  element_storage_dtor(storage);
  free(iter);
  free(storage);
}

TEST(read) {
  struct element_storage* storage;

  CHECK(element_storage_alloc(&storage));
  CHECK(element_storage_read(storage, "test.txt", 4096));

  enum ATTRIBUTE_TYPE attrs_type[3] = {ATTR_TYPE_STRING, ATTR_TYPE_INT64, ATTR_TYPE_INT32};
  struct element_type type1 = {
      .type_id = 0, .type_name = "Type 1", .attribute_count = 3, .attribute_types = attrs_type};

  struct storage_iterator* iter;
  CHECK(storage_iterator_alloc(&iter));

  CHECK(element_storage_get_element_type_iter(storage, iter) == 0);

  struct element_type real;
  CHECK(element_storage_get_element_type(storage, iter, &real) == 0);

  CHECK_ELEMENT_TYPE(real, type1);

  element_type_dtor(&real);

  element_storage_dtor(storage);
  free(iter);
  free(storage);
}

TEST(write_some) {
  struct element_storage* storage;

  CHECK(element_storage_alloc(&storage));
  CHECK(element_storage_ctor(storage, "test.txt", 4096));

  enum ATTRIBUTE_TYPE attrs_type1[3] = {ATTR_TYPE_STRING, ATTR_TYPE_INT64, ATTR_TYPE_INT32};
  enum ATTRIBUTE_TYPE attrs_type2[6] = {ATTR_TYPE_BOOL,   ATTR_TYPE_DOUBLE, ATTR_TYPE_BOOL,
                                        ATTR_TYPE_STRING, ATTR_TYPE_INT64,  ATTR_TYPE_INT32};
  enum ATTRIBUTE_TYPE attrs_type3[1] = {ATTR_TYPE_BOOL};

  struct element_type type1 = {
      .type_id = 0, .type_name = "Type 1", .attribute_count = 3, .attribute_types = attrs_type1};
  struct element_type type2 = {
      .type_id = 234, .type_name = "Second Type!!!", .attribute_count = 6, .attribute_types = attrs_type2};
  struct element_type type3 = {
      .type_id = 3, .type_name = "3\nthree\n3", .attribute_count = 1, .attribute_types = attrs_type3};

  struct link_type link_type1 = {.type_id = 0, .type_name = "LINK"};
  struct link_type link_type2 = {.type_id = 25, .type_name = "link\t2"};

  struct storage_iterator* element_iter;
  CHECK(storage_iterator_alloc(&element_iter));

  CHECK(element_storage_element_type_list_is_empty(storage));
  CHECK(element_storage_link_type_list_is_empty(storage));

  CHECK(element_storage_get_element_type_iter(storage, element_iter) == ERROR_LIST_IS_EMPTY);
  CHECK(element_storage_get_link_type_iter(storage, element_iter) == ERROR_LIST_IS_EMPTY);

  CHECK(element_storage_add_element_type(storage, &type1, NULL) == 0);

  CHECK(element_storage_get_element_type_iter(storage, element_iter) == 0);

  struct element_type real;
  CHECK(element_storage_get_element_type(storage, element_iter, &real) == 0);

  CHECK_ELEMENT_TYPE(real, type1);
  element_type_dtor(&real);

  CHECK(element_storage_remove_element_type(storage, element_iter) == 0);

  CHECK(element_storage_element_type_list_is_empty(storage));

  CHECK(element_storage_add_element_type(storage, &type2, NULL) == 0);
  CHECK(element_storage_add_element_type(storage, &type3, NULL) == 0);

  CHECK(!element_storage_element_type_list_is_empty(storage));

  CHECK(element_storage_get_element_type_iter(storage, element_iter) == 0);

  CHECK(element_storage_get_element_type(storage, element_iter, &real) == 0);
  bool is_type2 = (real.type_id == type2.type_id);

  if (is_type2) {
    CHECK_ELEMENT_TYPE(real, type2);
  } else {
    CHECK_ELEMENT_TYPE(real, type3);
  }
  element_type_dtor(&real);

  storage_iterator_next(element_iter);
  CHECK(element_storage_get_element_type(storage, element_iter, &real) == 0);

  if (is_type2) {
    CHECK_ELEMENT_TYPE(real, type3);
  } else {
    CHECK_ELEMENT_TYPE(real, type2);
  }
  element_type_dtor(&real);

  storage_iterator_next(element_iter);
  CHECK(element_storage_get_element_type(storage, element_iter, &real) == 0);

  if (is_type2) {
    CHECK_ELEMENT_TYPE(real, type2);
  } else {
    CHECK_ELEMENT_TYPE(real, type3);
  }
  element_type_dtor(&real);

  struct storage_iterator* link_iter;
  CHECK(storage_iterator_alloc(&link_iter));

  CHECK(element_storage_link_type_list_is_empty(storage));
  CHECK(element_storage_get_link_type_iter(storage, link_iter) == ERROR_LIST_IS_EMPTY);

  CHECK(element_storage_add_link_type(storage, &link_type1) == 0);
  CHECK(element_storage_add_link_type(storage, &link_type2) == 0);

  CHECK(!element_storage_link_type_list_is_empty(storage));

  CHECK(element_storage_get_link_type_iter(storage, link_iter) == 0);

  struct link_type real_link;
  CHECK(element_storage_get_link_type(storage, link_iter, &real_link) == 0);

  bool is_link_type1 = (real_link.type_id == link_type1.type_id);

  if (is_link_type1) {
    CHECK_LINK_TYPE(real_link, link_type1);
  } else {
    CHECK_LINK_TYPE(real_link, link_type2);
  }

  link_type_dtor(&real_link);

  storage_iterator_next(link_iter);
  CHECK(element_storage_get_link_type(storage, link_iter, &real_link) == 0);

  if (is_link_type1) {
    CHECK_LINK_TYPE(real_link, link_type2);
  } else {
    CHECK_LINK_TYPE(real_link, link_type1);
  }

  link_type_dtor(&real_link);

  storage_iterator_next(link_iter);
  CHECK(element_storage_get_link_type(storage, link_iter, &real_link) == 0);

  if (is_link_type1) {
    CHECK_LINK_TYPE(real_link, link_type1);
  } else {
    CHECK_LINK_TYPE(real_link, link_type2);
  }

  link_type_dtor(&real_link);

  free(link_iter);
  free(element_iter);
  element_storage_dtor(storage);
  free(storage);
}

TEST(read_some) {
  struct element_storage* storage;

  CHECK(element_storage_alloc(&storage));
  CHECK(element_storage_read(storage, "test.txt", 4096));

  enum ATTRIBUTE_TYPE attrs_type1[3] = {ATTR_TYPE_STRING, ATTR_TYPE_INT64, ATTR_TYPE_INT32};
  enum ATTRIBUTE_TYPE attrs_type2[6] = {ATTR_TYPE_BOOL,   ATTR_TYPE_DOUBLE, ATTR_TYPE_BOOL,
                                        ATTR_TYPE_STRING, ATTR_TYPE_INT64,  ATTR_TYPE_INT32};
  enum ATTRIBUTE_TYPE attrs_type3[1] = {ATTR_TYPE_BOOL};

  struct element_type type1 = {
      .type_id = 0, .type_name = "Type 1", .attribute_count = 3, .attribute_types = attrs_type1};
  struct element_type type2 = {
      .type_id = 234, .type_name = "Second Type!!!", .attribute_count = 6, .attribute_types = attrs_type2};
  struct element_type type3 = {
      .type_id = 3, .type_name = "3\nthree\n3", .attribute_count = 1, .attribute_types = attrs_type3};

  struct link_type link_type1 = {.type_id = 0, .type_name = "LINK"};
  struct link_type link_type2 = {.type_id = 25, .type_name = "link\t2"};

  struct storage_iterator* element_iter;
  CHECK(storage_iterator_alloc(&element_iter));

  CHECK(element_storage_get_element_type_iter(storage, element_iter) == 0);

  struct element_type real;
  CHECK(element_storage_get_element_type(storage, element_iter, &real) == 0);
  bool is_type2 = (real.type_id == type2.type_id);

  if (is_type2) {
    CHECK_ELEMENT_TYPE(real, type2);
  } else {
    CHECK_ELEMENT_TYPE(real, type3);
  }
  element_type_dtor(&real);

  storage_iterator_next(element_iter);
  CHECK(element_storage_get_element_type(storage, element_iter, &real) == 0);

  if (is_type2) {
    CHECK_ELEMENT_TYPE(real, type3);
  } else {
    CHECK_ELEMENT_TYPE(real, type2);
  }
  element_type_dtor(&real);

  storage_iterator_next(element_iter);
  CHECK(element_storage_get_element_type(storage, element_iter, &real) == 0);

  if (is_type2) {
    CHECK_ELEMENT_TYPE(real, type2);
  } else {
    CHECK_ELEMENT_TYPE(real, type3);
  }
  element_type_dtor(&real);

  struct storage_iterator* link_iter;
  CHECK(storage_iterator_alloc(&link_iter));

  CHECK(element_storage_get_link_type_iter(storage, link_iter) == 0);

  struct link_type real_link;
  CHECK(element_storage_get_link_type(storage, link_iter, &real_link) == 0);

  bool is_link_type1 = (real_link.type_id == link_type1.type_id);

  if (is_link_type1) {
    CHECK_LINK_TYPE(real_link, link_type1);
  } else {
    CHECK_LINK_TYPE(real_link, link_type2);
  }

  link_type_dtor(&real_link);

  storage_iterator_next(link_iter);
  CHECK(element_storage_get_link_type(storage, link_iter, &real_link) == 0);

  if (is_link_type1) {
    CHECK_LINK_TYPE(real_link, link_type2);
  } else {
    CHECK_LINK_TYPE(real_link, link_type1);
  }

  link_type_dtor(&real_link);

  storage_iterator_next(link_iter);
  CHECK(element_storage_get_link_type(storage, link_iter, &real_link) == 0);

  if (is_link_type1) {
    CHECK_LINK_TYPE(real_link, link_type1);
  } else {
    CHECK_LINK_TYPE(real_link, link_type2);
  }

  link_type_dtor(&real_link);

  free(link_iter);
  free(element_iter);
  element_storage_dtor(storage);
  free(storage);
}

void tear_down() {
#ifdef _WIN32
  remove("test.txt");
#endif /* _WIN32 */
}

int main() {
#ifndef DISABLED
  RUN_TEST(write);
  RUN_TEST(read);
  RUN_TEST(write_some);
  RUN_TEST(read_some);
  tear_down();
#else
  printf("Test disabled\n");
#endif /* DISABLED */
  return 0;
}