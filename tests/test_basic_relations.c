#include "query_processor.h"

#include "element_check.h"
#include "query.h"
#include "test_utils.h"

#include <stdio.h>
#include <time.h>

#define COUNT 1000

/*
Represented graphs

even (seed % 2 == 0)

      2-b->2
           ↑
0--a->1    b
      |    |
      --b->1

odd (seed % 2 == 1)

           2
           ↑
0--a->1    b
      |    |
      --b->1

*/

void create_graph(struct element_storage* storage, struct element_type types[3], struct link_type link_types[2],
                  int seed) {
  struct attribute root_attrs[3] = {{.type = ATTR_TYPE_BOOL, .value.as_bool = (seed % 3 == 0)},
                                    {.type = ATTR_TYPE_INT32, .value.as_int32 = seed},
                                    {.type = ATTR_TYPE_STRING, .value.as_string.data = "root"}};
  struct element root_element = {.element_type = &types[0], .attribute_count = 3, .attributes = root_attrs};
  struct attribute_pattern root_attrs_pattern = {
      .node_type = ATTRIBUTE_PATTERN_COND,
      .args.as_cond = {
          .cond = ATTRIBUTE_PATTERN_COND_EQ, .attr_id = 1, .attr = {.type = ATTR_TYPE_INT32, .value.as_int32 = seed}}};

  struct element_pattern root_pattern = {.element_type = &types[0], .attr_pattern = &root_attrs_pattern};

  struct query q = {.type = QUERY_TYPE_CREATE, .args.as_create.element = &root_element};
  CHECK(process_query(storage, &q, NULL) == 0);

  struct attribute type1_attrs1[2] = {{.type = ATTR_TYPE_INT64, .value.as_int64 = seed + 1},
                                      {.type = ATTR_TYPE_DOUBLE, .value.as_double = seed / 2.0}};
  struct element el1_type1 = {.element_type = &types[1], .attribute_count = 2, .attributes = type1_attrs1};
  struct attribute_pattern el1_attrs_pattern = {
      .node_type = ATTRIBUTE_PATTERN_COND,
      .args.as_cond = {.cond = ATTRIBUTE_PATTERN_COND_EQ,
                       .attr_id = 0,
                       .attr = {.type = ATTR_TYPE_INT64, .value.as_int64 = seed + 1}}};

  struct element_pattern el1_pattern = {.element_type = &types[1], .attr_pattern = &el1_attrs_pattern};

  q.args.as_create.element = &el1_type1;
  CHECK(process_query(storage, &q, NULL) == 0);

  struct attribute type1_attrs2[2] = {{.type = ATTR_TYPE_INT64, .value.as_int64 = seed - 1},
                                      {.type = ATTR_TYPE_DOUBLE, .value.as_double = seed}};
  struct element el2_type1 = {.element_type = &types[1], .attribute_count = 2, .attributes = type1_attrs2};

  struct attribute_pattern el2_attrs_pattern = {
      .node_type = ATTRIBUTE_PATTERN_COND,
      .args.as_cond = {.cond = ATTRIBUTE_PATTERN_COND_EQ,
                       .attr_id = 0,
                       .attr = {.type = ATTR_TYPE_INT64, .value.as_int64 = seed - 1}}};

  struct element_pattern el2_pattern = {.element_type = &types[1], .attr_pattern = &el2_attrs_pattern};

  q.args.as_create.element = &el2_type1;
  CHECK(process_query(storage, &q, NULL) == 0);

  struct attribute type2_attrs[3] = {{.type = ATTR_TYPE_INT32, .value.as_int32 = seed + 2},
                                     {.type = ATTR_TYPE_BOOL, .value.as_bool = (seed % 6 < 3)},
                                     {.type = ATTR_TYPE_STRING, .value.as_string = "destination"}};
  struct element el_type2 = {.element_type = &types[2], .attribute_count = 3, .attributes = type2_attrs};

  struct attribute_pattern el_attrs_pattern = {
      .node_type = ATTRIBUTE_PATTERN_COND,
      .args.as_cond = {.cond = ATTRIBUTE_PATTERN_COND_EQ,
                       .attr_id = 0,
                       .attr = {.type = ATTR_TYPE_INT32, .value.as_int32 = seed + 2}}};
  struct element_pattern el_pattern = {.element_type = &types[2], .attr_pattern = &el_attrs_pattern};

  q.args.as_create.element = &el_type2;
  CHECK(process_query(storage, &q, NULL) == 0);

  q.type = QUERY_TYPE_LINK;

  q.args.as_link =
      (struct link_args){.source_pattern = &root_pattern, .dst_pattern = &el1_pattern, .link_type = &link_types[0]};

  CHECK(process_query(storage, &q, NULL) == 0);

  q.args.as_link =
      (struct link_args){.source_pattern = &el1_pattern, .dst_pattern = &el2_pattern, .link_type = &link_types[1]};
  CHECK(process_query(storage, &q, NULL) == 0);

  q.args.as_link =
      (struct link_args){.source_pattern = &el2_pattern, .dst_pattern = &el_pattern, .link_type = &link_types[1]};
  CHECK(process_query(storage, &q, NULL) == 0);

  if (seed % 2 == 0) {
    struct attribute type2_attrs2[3] = {{.type = ATTR_TYPE_INT32, .value.as_int32 = seed + 3},
                                        {.type = ATTR_TYPE_BOOL, .value.as_bool = (seed % 4 < 3)},
                                        {.type = ATTR_TYPE_STRING, .value.as_string = "second_root"}};
    struct element el2_type2 = {.element_type = &types[2], .attribute_count = 3, .attributes = type2_attrs2};

    q.type = QUERY_TYPE_CREATE;
    q.args.as_create.element = &el2_type2;
    CHECK(process_query(storage, &q, NULL) == 0);

    struct attribute_pattern type2_attrs2_pattern = {
        .node_type = ATTRIBUTE_PATTERN_COND,
        .args.as_cond = {.cond = ATTRIBUTE_PATTERN_COND_EQ,
                         .attr_id = 0,
                         .attr = {.type = ATTR_TYPE_INT32, .value.as_int32 = seed + 3}}};
    struct element_pattern el2_type2_pattern = {.element_type = &types[2], .attr_pattern = &type2_attrs2_pattern};

    q.type = QUERY_TYPE_LINK;
    q.args.as_link = (struct link_args){
        .source_pattern = &el2_type2_pattern, .dst_pattern = &el_pattern, .link_type = &link_types[1]};
    CHECK(process_query(storage, &q, NULL) == 0);
  }
}

void create_false_roots(struct element_storage* storage, struct element_type* type, int count) {
  struct attribute false_root_attrs[3] = {{.type = ATTR_TYPE_BOOL, .value.as_bool = false},
                                          {.type = ATTR_TYPE_INT32, .value.as_int32 = -1},
                                          {.type = ATTR_TYPE_STRING, .value.as_string.data = "root"}};
  struct element false_root = {.element_type = type, .attribute_count = 3, .attributes = false_root_attrs};
  struct query q = {.type = QUERY_TYPE_CREATE, .args.as_create.element = &false_root};

  clock_t start = clock();
  for (int i = 0; i < count; i++) {
    false_root_attrs[0].value.as_bool = (i % 3 == 0);
    false_root_attrs[1].value.as_int32 = i;
    CHECK(process_query(storage, &q, NULL) == 0);
  }
  clock_t end = clock();
  double elapsed_time = (end - start) / (double)CLOCKS_PER_SEC;
  printf("Created %d elements within %lf seconds\n", count, elapsed_time);
}

int count_items(struct element_storage* storage, struct element_pattern* pattern) {
  struct query q = {.type = QUERY_TYPE_MATCH, .args.as_match.pattern = pattern};

  struct query_result* q_res;
  CHECK(query_result_alloc(&q_res));

  error_code error = process_query(storage, &q, q_res);
  int match = 0;
  while (error == 0) {
    match++;
    error = query_result_next(q_res);
  }
  CHECK(error == ERROR_HAS_NO_NEXT_ELEMENT);

  query_result_dtor(q_res);
  free(q_res);

  return match;
}

int root_match(struct element_storage* storage, struct element_type* type) {
  struct element_pattern all_elements = {.element_type = type};
  return count_items(storage, &all_elements);
}

int deep_root_match(struct element_storage* storage, struct element_type types[3], struct link_type link_types[2],
                    bool is_even) {
  struct element_pattern el_type2_pattern = {.element_type = &types[2]};
  struct element_pattern el2_type2_pattern = {.element_type = &types[2]};
  struct element_pattern el2_type1_pattern = {.element_type = &types[1]};
  struct element_pattern el1_type1_pattern = {.element_type = &types[1]};
  struct element_pattern root_pattern = {.element_type = &types[0]};

  struct link_pattern el_type2_links_in[2] = {{.link_type = link_types[1].type_id, .target = &el2_type1_pattern},
                                              {.link_type = link_types[1].type_id, .target = &el2_type2_pattern}};
  el_type2_pattern.links_in = el_type2_links_in;
  el_type2_pattern.links_in_cnt = is_even ? 2 : 1;

  struct link_pattern el2_type1_links_out[1] = {{.link_type = link_types[1].type_id, .target = &el_type2_pattern}};
  el2_type1_pattern.links_out_cnt = 1;
  el2_type1_pattern.links_out = el2_type1_links_out;

  struct link_pattern el1_type1_links_out[1] = {{.link_type = link_types[1].type_id, .target = &el2_type1_pattern}};
  el1_type1_pattern.links_out_cnt = 1;
  el1_type1_pattern.links_out = el1_type1_links_out;

  struct link_pattern root_links_out[1] = {{.link_type = link_types[0].type_id, .target = &el1_type1_pattern}};
  root_pattern.links_out_cnt = 1;
  root_pattern.links_out = root_links_out;

  return count_items(storage, &root_pattern);
}

int delete_some_el1_type1(struct element_storage* storage, struct element_type types[3],
                          struct link_type link_types[2]) {
  struct element_pattern el1_pattern = {.element_type = &types[1]};

  struct attribute_pattern root_attr_pattern = {
      .node_type = ATTRIBUTE_PATTERN_COND,
      .args.as_cond = {
          .cond = ATTRIBUTE_PATTERN_COND_EQ, .attr_id = 0, .attr = {.type = ATTR_TYPE_BOOL, .value.as_bool = false}}};

  struct element_pattern root_pattern = {.element_type = &types[0], .attr_pattern = &root_attr_pattern};

  struct link_pattern el1_links_in_pattern[1] = {{.target = &root_pattern, .link_type = link_types[0].type_id}};
  el1_pattern.links_in_cnt = 1;
  el1_pattern.links_in = el1_links_in_pattern;

  int deleted = count_items(storage, &el1_pattern);
  CHECK(deleted == (COUNT - (COUNT - 1) / 3) - 1);

  struct query q = {.type = QUERY_TYPE_DELETE, .args.as_delete.pattern = &el1_pattern};

  clock_t start = clock();
  CHECK(process_query(storage, &q, NULL) == 0);
  clock_t end = clock();
  double elapsed_time = (end - start) / (double)CLOCKS_PER_SEC;
  printf("Deleted %d of %d elements of type0 within %lf seconds\n", deleted, COUNT + COUNT, elapsed_time);

  return deleted;
}

int count_sticks(struct element_storage* storage, struct element_type* src, struct element_type* dst,
                 struct link_type* link_type) {
  struct element_pattern src_pattern = {.element_type = src};
  struct element_pattern dst_pattern = {.element_type = dst};

  struct link_pattern src_out_links[1] = {{.link_type = link_type->type_id, .target = &dst_pattern}};
  struct link_pattern dst_in_links[1] = {{.link_type = link_type->type_id, .target = &src_pattern}};

  src_pattern.links_out_cnt = 1;
  src_pattern.links_out = src_out_links;

  dst_pattern.links_in_cnt = 1;
  dst_pattern.links_in = dst_in_links;

  return count_items(storage, &src_pattern);
}

TEST(simple) {
  struct element_storage* storage;
  CHECK(element_storage_alloc(&storage));
  CHECK(element_storage_ctor(storage, "test.txt", 4096));

  enum ATTRIBUTE_TYPE attrs_types0[3] = {ATTR_TYPE_BOOL, ATTR_TYPE_INT32, ATTR_TYPE_STRING};
  enum ATTRIBUTE_TYPE attrs_types1[2] = {ATTR_TYPE_INT64, ATTR_TYPE_DOUBLE};
  enum ATTRIBUTE_TYPE attrs_types2[3] = {ATTR_TYPE_INT32, ATTR_TYPE_BOOL, ATTR_TYPE_STRING};

  struct element_type types[3] = {
      {.type_id = 0, .type_name = "0", .attribute_count = 3, .attribute_types = attrs_types0},
      {.type_id = 1, .type_name = "1", .attribute_count = 2, .attribute_types = attrs_types1},
      {.type_id = 2, .type_name = "2", .attribute_count = 3, .attribute_types = attrs_types2}};

  for (int i = 0; i < 3; i++) {
    CHECK(element_storage_add_element_type(storage, &types[i], NULL) == 0);
  }

  struct link_type link_types[2] = {{.type_id = 0, .type_name = "A"}, {.type_id = 1, .type_name = "B"}};

  clock_t start = clock();
  for (int i = 0; i < COUNT; i++) {
    create_graph(storage, types, link_types, i);
  }
  clock_t end = clock();
  double elapsed_time = (end - start) / (double)CLOCKS_PER_SEC;
  printf("Created %d graphs within %lf seconds\n", COUNT, elapsed_time);
  create_false_roots(storage, &types[0], COUNT);

  struct element_pattern el1_pattern = {.element_type = &types[1]};
  struct link_pattern link1_pattern = {.link_type = link_types[0].type_id, .target = &el1_pattern};
  struct link_pattern root_links_out[1] = {link1_pattern};
  struct element_pattern root_pattern = {.element_type = &types[0], .links_out_cnt = 1, .links_out = root_links_out};

  struct query_result* q_res;
  query_result_alloc(&q_res);

  struct query q = {.type = QUERY_TYPE_MATCH, .args.as_match.pattern = &root_pattern};

  error_code error = process_query(storage, &q, q_res);
  int match = 0;

  while (error == 0) {
    match++;
    error = query_result_next(q_res);
  }
  query_result_dtor(q_res);
  printf("Matched %d of %d elements of type0\n", match, match + COUNT);

  CHECK(error == ERROR_HAS_NO_NEXT_ELEMENT);
  CHECK(match == COUNT);

  match = root_match(storage, &types[0]);
  printf("Matched %d of %d elements of type0\n", match, COUNT + COUNT);

  CHECK(match == COUNT + COUNT);

  element_storage_dtor(storage);
  element_storage_read(storage, "test.txt", 4096);

  match = deep_root_match(storage, types, link_types, false);
  CHECK(match == COUNT);

  match = deep_root_match(storage, types, link_types, true);
  CHECK(match == COUNT / 2);

  int deleted = delete_some_el1_type1(storage, types, link_types);

  match = root_match(storage, types);
  CHECK(match == COUNT + COUNT);

  match = deep_root_match(storage, types, link_types, false);
  CHECK(match == COUNT - deleted);

  match = count_sticks(storage, &types[1], &types[2], &link_types[1]);
  CHECK(match == COUNT);

  match = count_sticks(storage, &types[2], &types[2], &link_types[1]);
  CHECK(match == COUNT / 2);

  free(q_res);
  element_storage_dtor(storage);
  free(storage);
}

void tear_down() {
  remove("test.txt");
}

int main() {
#ifndef DISABLED
  RUN_TEST(simple);
  tear_down();
#else
  printf("Test disabled\n");
#endif /* DISABLED */
  return 0;
}