#pragma once

#ifndef _TEST_UTILS_H_
#define _TEST_UTILS_H_

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define CHECK(statement)                                                  \
  do {                                                                    \
    if (!(statement)) {                                                   \
      fprintf(stderr, "Statement failed at %s:%d\n", __FILE__, __LINE__); \
      abort();                                                            \
    }                                                                     \
  } while (false)

#define TEST(name) void test_##name()

#define RUN_TEST(name)                     \
  do {                                     \
    printf("------------------------\n");  \
    printf("Running test " #name "...\n"); \
    test_##name();                         \
    printf("Success!\n");                  \
    printf("------------------------\n");  \
  } while (0)

#endif
