#pragma once

#ifndef _ELEMENT_CHECK_H_
#define _ELEMENT_CHECK_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "element.h"
#include "schema.h"

bool is_type_equals(struct element_type* lhs, struct element_type* rhs);

bool is_attrs_equals(struct attribute* lhs, struct attribute* rhs, attribute_count_t cnt);

void dump_element(struct element* el);

#endif /* _ELEMENT_CHECK_H_ */