#pragma once

#ifndef _OFFSET_TEST_MEMORY_MANAGER_INTERFACE_H_
#define _OFFSET_TEST_MEMORY_MANAGER_INTERFACE_H_

#include <stdbool.h>

#include "offset_memory_manager_extended_interface.h"

struct offset_test_memory_manager;

bool offset_test_memory_manager_alloc(struct offset_test_memory_manager** ptr);

void offset_test_memory_manager_ctor(struct offset_test_memory_manager* mem_manager, mem_size_t size);
void offset_test_memory_manager_dtor(struct offset_test_memory_manager* mem_manager);

#endif /* _OFFSET_TEST_MEMORY_MANAGER_INTERFACE_H_ */
