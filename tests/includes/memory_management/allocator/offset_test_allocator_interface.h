#pragma once

#ifndef _OFFSET_TEST_ALLOCATOR_INTERFACE_H_
#define _OFFSET_TEST_ALLOCATOR_INTERFACE_H_

#include <stdbool.h>

#include "common_types.h"

struct offset_test_allocator;

bool offset_test_allocator_alloc(struct offset_test_allocator** ptr);

void offset_test_allocator_ctor(struct offset_test_allocator* alloc, mem_size_t alloc_size);
void offset_test_allocator_dtor(struct offset_test_allocator* alloc);

#endif /* _OFFSET_TEST_ALLOCATOR_INTERFACE_H_ */
