#include "element_storage_api.h"

#include "element_check.h"
#include "test_utils.h"

#include <string.h>

TEST(write) {
  struct element_storage* storage;

  CHECK(element_storage_alloc(&storage));
  CHECK(element_storage_ctor(storage, "test.txt", 4096));

  enum ATTRIBUTE_TYPE attrs_type1[3] = {ATTR_TYPE_STRING, ATTR_TYPE_INT64, ATTR_TYPE_INT32};
  enum ATTRIBUTE_TYPE attrs_type2[6] = {ATTR_TYPE_BOOL,   ATTR_TYPE_DOUBLE, ATTR_TYPE_BOOL,
                                        ATTR_TYPE_STRING, ATTR_TYPE_INT64,  ATTR_TYPE_INT32};

  struct element_type type1 = {
      .type_id = 0, .type_name = "Type 1", .attribute_count = 3, .attribute_types = attrs_type1};
  struct element_type type2 = {
      .type_id = 234, .type_name = "Second Type!!!", .attribute_count = 6, .attribute_types = attrs_type2};

  struct attribute elem1_attrs[3] = {
      {.type = ATTR_TYPE_STRING,
       .value.as_string.data = "DATA1 DATA2 DATA3 DATA4 DATA5 DATA6 DATA7 DATA8 DATA9 DATA10 DATA11"},
      {.type = ATTR_TYPE_INT64, .value.as_int64 = INT64_MAX},
      {.type = ATTR_TYPE_INT32, .value.as_int32 = INT32_MIN}};

  struct attribute elem2_attrs[3] = {{.type = ATTR_TYPE_STRING, .value.as_string.data = ""},
                                     {.type = ATTR_TYPE_INT64, .value.as_int64 = 0},
                                     {.type = ATTR_TYPE_INT32, .value.as_int32 = 34}};

  struct attribute elem3_attrs[6] = {{.type = ATTR_TYPE_BOOL, .value.as_bool = false},
                                     {.type = ATTR_TYPE_DOUBLE, .value.as_double = -3.14159265358979},
                                     {.type = ATTR_TYPE_BOOL, .value.as_bool = true},
                                     {.type = ATTR_TYPE_STRING, .value.as_string.data = "string"},
                                     {.type = ATTR_TYPE_INT64, .value.as_int64 = 100000},
                                     {.type = ATTR_TYPE_INT32, .value.as_int32 = 32}};

  struct element elem1 = {
      .element_type = &type1, .owns_type_ptr = false, .attribute_count = 3, .attributes = elem1_attrs};
  struct element elem2 = {
      .element_type = &type1, .owns_type_ptr = false, .attribute_count = 3, .attributes = elem2_attrs};
  struct element elem3 = {
      .element_type = &type2, .owns_type_ptr = false, .attribute_count = 6, .attributes = elem3_attrs};

  struct link_type link_type1 = {.type_id = 0, .type_name = "LINK"};
  struct link_type link_type2 = {.type_id = 25, .type_name = "link\t2"};

  struct storage_iterator* element_type1_iter;
  CHECK(storage_iterator_alloc(&element_type1_iter));

  struct storage_iterator* element_type2_iter;
  CHECK(storage_iterator_alloc(&element_type2_iter));

  CHECK(element_storage_add_element_type(storage, &type1, element_type1_iter) == 0);
  CHECK(element_storage_add_element_type(storage, &type2, element_type2_iter) == 0);

  struct storage_iterator* element_iter;
  CHECK(storage_iterator_alloc(&element_iter));
  CHECK(element_storage_get_element_iter(storage, element_type1_iter, element_iter) == ERROR_LIST_IS_EMPTY);

  CHECK(element_storage_add_element_of_type(storage, element_type1_iter, &elem1) == 0);
  CHECK(element_storage_add_element_of_type(storage, element_type1_iter, &elem2) == 0);

  CHECK(element_storage_get_element_iter(storage, element_type1_iter, element_iter) == 0);

  struct element real = {};
  CHECK(element_storage_get_element_attrs(storage, element_iter, &real) == 0);

  bool is_elem1 = (real.attributes[1].value.as_int64 == elem1.attributes[1].value.as_int64);
  if (is_elem1) {
    CHECK(is_attrs_equals(real.attributes, elem1.attributes, type1.attribute_count));
  } else {
    CHECK(is_attrs_equals(real.attributes, elem2.attributes, type1.attribute_count));
  }

  element_dtor(&real);
  real = (struct element){};

  storage_iterator_next(element_iter);

  CHECK(element_storage_get_element(storage, element_iter, &real) == 0);
  CHECK(is_type_equals(real.element_type, &type1));

  if (is_elem1) {
    CHECK(is_attrs_equals(real.attributes, elem2.attributes, type1.attribute_count));
  } else {
    CHECK(is_attrs_equals(real.attributes, elem1.attributes, type1.attribute_count));
  }

  element_dtor(&real);
  real = (struct element){};

  storage_iterator_next(element_iter);

  CHECK(element_storage_get_element(storage, element_iter, &real) == 0);
  CHECK(is_type_equals(real.element_type, &type1));

  if (is_elem1) {
    CHECK(is_attrs_equals(real.attributes, elem1.attributes, type1.attribute_count));
  } else {
    CHECK(is_attrs_equals(real.attributes, elem2.attributes, type1.attribute_count));
  }

  if (!is_elem1) {
    storage_iterator_next(element_iter);
  }

  element_dtor(&real);
  real = (struct element){};

  struct storage_iterator* dst_element_iter;
  CHECK(storage_iterator_alloc(&dst_element_iter));

  CHECK(element_storage_get_element_iter(storage, element_type2_iter, dst_element_iter) == ERROR_LIST_IS_EMPTY);
  CHECK(element_storage_add_element_of_type(storage, element_type2_iter, &elem3) == 0);
  CHECK(element_storage_get_element_iter(storage, element_type2_iter, dst_element_iter) == 0);

  CHECK(element_storage_get_element(storage, dst_element_iter, &real) == 0);

  CHECK(is_type_equals(real.element_type, &type2));
  CHECK(is_attrs_equals(real.attributes, elem3_attrs, type2.attribute_count));

  element_dtor(&real);
  real = (struct element){};

  storage_iterator_next(dst_element_iter);

  CHECK(element_storage_get_element_attrs(storage, dst_element_iter, &real) == 0);

  CHECK(is_attrs_equals(real.attributes, elem3_attrs, type2.attribute_count));
  element_dtor(&real);
  real = (struct element){};

  struct link link1 = {.dst = &elem3, .link_type = link_type1.type_id};
  struct link link2 = {.dst = &elem2, .link_type = link_type2.type_id};
  struct link link3 = {.dst = &elem3, .link_type = link_type2.type_id};

  CHECK(element_storage_add_link(storage, element_iter, dst_element_iter, link1.link_type) == 0);

  CHECK(element_storage_get_element_iter(storage, element_type1_iter, dst_element_iter) == 0);

  if (storage_iterator_equals(dst_element_iter, element_iter)) {
    storage_iterator_next(dst_element_iter);
  }

  CHECK(element_storage_add_link(storage, element_iter, dst_element_iter, link2.link_type) == 0);

  CHECK(element_storage_get_element_iter(storage, element_type2_iter, element_iter) == 0);

  CHECK(element_storage_add_link(storage, dst_element_iter, element_iter, link3.link_type) == 0);

  free(dst_element_iter);
  free(element_iter);

  element_storage_dtor(storage);

  element_storage_read(storage, "test.txt", 4096);
  // CHECK 1-(1)->3 and 1-(2)->2-(1)->3

  CHECK(element_storage_get_element_type_iter(storage, element_type1_iter) == 0);
  storage_iterator_copy(element_type1_iter, element_type2_iter);

  struct element_type real_type = {};
  CHECK(element_storage_get_element_type(storage, element_type1_iter, &real_type) == 0);

  if (real_type.type_id != type1.type_id) {
    storage_iterator_next(element_type1_iter);
  } else {
    storage_iterator_next(element_type2_iter);
  }

  element_type_dtor(&real_type);
  real_type = (struct element_type){};

  struct storage_iterator* iter1 = NULL;
  CHECK(storage_iterator_alloc(&iter1));

  struct storage_iterator* iter2 = NULL;
  CHECK(storage_iterator_alloc(&iter2));

  struct storage_iterator* iter3 = NULL;
  CHECK(storage_iterator_alloc(&iter3));

  CHECK(element_storage_get_element_iter(storage, element_type1_iter, iter1) == 0);

  CHECK(element_storage_get_element_attrs(storage, iter1, &real) == 0);
  if (real.attributes[1].value.as_int64 != elem1.attributes[1].value.as_int64) {
    storage_iterator_next(iter1);
  }
  element_dtor(&real);
  real = (struct element){};

  CHECK(element_storage_get_element_attrs(storage, iter1, &real) == 0);
  is_attrs_equals(real.attributes, elem1.attributes, type1.attribute_count);

  CHECK(element_storage_get_element_links_in_iter(storage, iter1, iter3) == ERROR_LIST_IS_EMPTY);
  CHECK(element_storage_get_element_links_out_iter(storage, iter1, iter3) == 0);

  storage_iterator_copy(iter3, iter2);

  link_type_id_t real_link_type;
  CHECK(element_storage_get_type_of_link(storage, iter3, &real_link_type) == 0);

  if (real_link_type == link1.link_type) {
    storage_iterator_next(iter2);
  } else {
    storage_iterator_next(iter3);
  }

  element_type_id_t real_dst_elem_type;
  CHECK(element_storage_get_type_of_link_dst_element(storage, iter3, &real_dst_elem_type) == 0);
  CHECK(real_dst_elem_type == type2.type_id);
  CHECK(element_storage_get_type_of_link_dst_element(storage, iter2, &real_dst_elem_type) == 0);
  CHECK(real_dst_elem_type == type1.type_id);

  CHECK(element_storage_get_link_dst_element_iter(storage, iter3, iter3) == 0);
  CHECK(element_storage_get_link_dst_element_iter(storage, iter2, iter2) == 0);

  element_dtor(&real);
  real = (struct element){};

  CHECK(element_storage_get_element(storage, iter3, &real) == 0);
  CHECK(is_type_equals(real.element_type, elem3.element_type));
  CHECK(is_attrs_equals(real.attributes, elem3.attributes, elem3.element_type->attribute_count));

  element_dtor(&real);
  real = (struct element){};

  CHECK(element_storage_get_element(storage, iter2, &real) == 0);
  CHECK(is_type_equals(real.element_type, elem2.element_type));
  CHECK(is_attrs_equals(real.attributes, elem2.attributes, elem2.element_type->attribute_count));

  element_dtor(&real);
  real = (struct element){};

  struct storage_iterator* link_iter;
  CHECK(storage_iterator_alloc(&link_iter));

  CHECK(element_storage_get_element_links_in_iter(storage, iter2, link_iter) == 0);

  CHECK(element_storage_get_type_of_link(storage, link_iter, &real_link_type) == 0);
  CHECK(real_link_type == link2.link_type);
  CHECK(element_storage_get_link_dst_element_iter(storage, link_iter, link_iter) == 0);
  CHECK(storage_iterator_equals(link_iter, iter1));

  CHECK(element_storage_get_element_links_out_iter(storage, iter2, link_iter) == 0);
  CHECK(element_storage_get_type_of_link(storage, link_iter, &real_link_type) == 0);
  CHECK(real_link_type == link3.link_type);
  CHECK(element_storage_get_link_dst_element_iter(storage, link_iter, link_iter) == 0);
  CHECK(storage_iterator_equals(link_iter, iter3));

  CHECK(element_storage_get_element_links_in_iter(storage, iter3, link_iter) == 0);
  CHECK(element_storage_get_type_of_link(storage, link_iter, &real_link_type) == 0);

  if (real_link_type == link2.link_type) {
    CHECK(element_storage_get_link_dst_element_iter(storage, link_iter, link_iter) == 0);
    CHECK(storage_iterator_equals(link_iter, iter2));

    CHECK(element_storage_get_element_links_in_iter(storage, iter3, link_iter) == 0);

    storage_iterator_next(link_iter);

    CHECK(element_storage_get_type_of_link(storage, link_iter, &real_link_type) == 0);
    CHECK(real_link_type == link1.link_type);
    CHECK(element_storage_get_link_dst_element_iter(storage, link_iter, link_iter) == 0);
    CHECK(storage_iterator_equals(link_iter, iter1));
  } else {
    CHECK(real_link_type == link1.link_type);
    CHECK(element_storage_get_link_dst_element_iter(storage, link_iter, link_iter) == 0);
    CHECK(storage_iterator_equals(link_iter, iter1));

    CHECK(element_storage_get_element_links_in_iter(storage, iter3, link_iter) == 0);

    storage_iterator_next(link_iter);

    CHECK(element_storage_get_type_of_link(storage, link_iter, &real_link_type) == 0);
    CHECK(real_link_type == link2.link_type);
    CHECK(element_storage_get_link_dst_element_iter(storage, link_iter, link_iter) == 0);
    CHECK(storage_iterator_equals(link_iter, iter1));
  }

  CHECK(element_storage_get_element(storage, iter2, &real) == 0);
  CHECK(is_type_equals(real.element_type, elem2.element_type));
  CHECK(is_attrs_equals(real.attributes, elem2.attributes, elem2.element_type->attribute_count));

  element_dtor(&real);
  real = (struct element){};

  CHECK(element_storage_remove_element(storage, element_type1_iter, iter2) == 0);

  free(link_iter);
  free(iter1);
  free(iter2);
  free(iter3);
  free(element_type1_iter);
  free(element_type2_iter);
  element_storage_dtor(storage);
  free(storage);
}

TEST(read) {
  struct element_storage* storage;

  CHECK(element_storage_alloc(&storage));
  CHECK(element_storage_read(storage, "test.txt", 4096));

  enum ATTRIBUTE_TYPE attrs_type1[3] = {ATTR_TYPE_STRING, ATTR_TYPE_INT64, ATTR_TYPE_INT32};
  enum ATTRIBUTE_TYPE attrs_type2[6] = {ATTR_TYPE_BOOL,   ATTR_TYPE_DOUBLE, ATTR_TYPE_BOOL,
                                        ATTR_TYPE_STRING, ATTR_TYPE_INT64,  ATTR_TYPE_INT32};

  struct element_type type1 = {
      .type_id = 0, .type_name = "Type 1", .attribute_count = 3, .attribute_types = attrs_type1};
  struct element_type type2 = {
      .type_id = 234, .type_name = "Second Type!!!", .attribute_count = 6, .attribute_types = attrs_type2};

  struct attribute elem1_attrs[3] = {
      {.type = ATTR_TYPE_STRING,
       .value.as_string.data = "DATA1 DATA2 DATA3 DATA4 DATA5 DATA6 DATA7 DATA8 DATA9 DATA10 DATA11"},
      {.type = ATTR_TYPE_INT64, .value.as_int64 = INT64_MAX},
      {.type = ATTR_TYPE_INT32, .value.as_int32 = INT32_MIN}};

  struct attribute elem3_attrs[6] = {{.type = ATTR_TYPE_BOOL, .value.as_bool = false},
                                     {.type = ATTR_TYPE_DOUBLE, .value.as_double = -3.14159265358979},
                                     {.type = ATTR_TYPE_BOOL, .value.as_bool = true},
                                     {.type = ATTR_TYPE_STRING, .value.as_string.data = "string"},
                                     {.type = ATTR_TYPE_INT64, .value.as_int64 = 100000},
                                     {.type = ATTR_TYPE_INT32, .value.as_int32 = 32}};

  struct element elem1 = {.element_type = &type1, .owns_type_ptr = false, .attributes = elem1_attrs};
  struct element elem3 = {.element_type = &type2, .owns_type_ptr = false, .attributes = elem3_attrs};

  struct link_type link_type1 = {.type_id = 0, .type_name = "LINK"};
  struct link_type link_type2 = {.type_id = 25, .type_name = "link\t2"};

  struct storage_iterator* element_type1_iter;
  CHECK(storage_iterator_alloc(&element_type1_iter));

  struct storage_iterator* element_type2_iter;
  CHECK(storage_iterator_alloc(&element_type2_iter));

  CHECK(element_storage_get_element_type_iter(storage, element_type1_iter) == 0);
  storage_iterator_copy(element_type1_iter, element_type2_iter);

  struct element_type real_type;
  CHECK(element_storage_get_element_type(storage, element_type1_iter, &real_type) == 0);
  if (real_type.type_id == type1.type_id) {
    storage_iterator_next(element_type2_iter);
  } else {
    storage_iterator_next(element_type1_iter);
  }
  element_type_dtor(&real_type);

  struct storage_iterator* iter1;
  CHECK(storage_iterator_alloc(&iter1));

  CHECK(element_storage_get_element_iter(storage, element_type1_iter, iter1) == 0);

  struct storage_iterator* iter2;
  CHECK(storage_iterator_alloc(&iter2));

  storage_iterator_copy(iter1, iter2);
  storage_iterator_next(iter1);

  CHECK(storage_iterator_equals(iter1, iter2));

  struct element real = {};
  CHECK(element_storage_get_element(storage, iter1, &real) == 0);
  CHECK(is_type_equals(real.element_type, elem1.element_type));
  CHECK(is_attrs_equals(real.attributes, elem1.attributes, elem1.element_type->attribute_count));

  element_dtor(&real);
  real = (struct element){};

  storage_iterator_copy(iter1, iter2);
  storage_iterator_next(iter1);

  CHECK(storage_iterator_equals(iter1, iter2));

  CHECK(element_storage_get_element_links_in_iter(storage, iter1, iter2) == ERROR_LIST_IS_EMPTY);
  CHECK(element_storage_get_element_links_out_iter(storage, iter1, iter2) == 0);

  struct storage_iterator* iter3;
  CHECK(storage_iterator_alloc(&iter3));
  storage_iterator_copy(iter2, iter3);
  // storage_iterator_next(iter3);
  storage_iterator_next(iter2);

  CHECK(storage_iterator_equals(iter2, iter3));

  link_type_id_t real_link_type;
  CHECK(element_storage_get_type_of_link(storage, iter2, &real_link_type) == 0);

  CHECK(real_link_type == link_type1.type_id);

  element_type_id_t real_dst_element_type;
  CHECK(element_storage_get_type_of_link_dst_element(storage, iter2, &real_dst_element_type) == 0);
  CHECK(real_dst_element_type == elem3.element_type->type_id);

  CHECK(element_storage_get_element_iter(storage, element_type2_iter, iter3) == 0);
  CHECK(element_storage_get_link_dst_element_iter(storage, iter2, iter2) == 0);

  CHECK(storage_iterator_equals(iter2, iter3));

  storage_iterator_next(iter3);

  CHECK(storage_iterator_equals(iter2, iter3));

  CHECK(element_storage_get_element(storage, iter3, &real) == 0);
  CHECK(is_type_equals(real.element_type, elem3.element_type));
  CHECK(is_attrs_equals(real.attributes, elem3.attributes, elem3.element_type->attribute_count));

  element_dtor(&real);
  real = (struct element){};

  CHECK(element_storage_get_element_links_in_iter(storage, iter3, iter2) == 0);
  storage_iterator_copy(iter2, iter3);

  storage_iterator_next(iter3);

  CHECK(storage_iterator_equals(iter3, iter2));

  CHECK(element_storage_get_type_of_link(storage, iter2, &real_link_type) == 0);

  CHECK(real_link_type == link_type1.type_id);

  CHECK(element_storage_get_type_of_link_dst_element(storage, iter2, &real_dst_element_type) == 0);
  CHECK(real_dst_element_type == elem1.element_type->type_id);

  CHECK(element_storage_get_link_dst_element_iter(storage, iter2, iter2) == 0);
  CHECK(storage_iterator_equals(iter2, iter1));

  free(iter3);
  free(iter2);
  free(iter1);
  free(element_type2_iter);
  free(element_type1_iter);
  element_storage_dtor(storage);
  free(storage);
}

void tear_down() {
#ifdef _WIN32
  remove("test.txt");
#endif /* _WIN32 */
}

int main() {
#ifndef DISABLED
  RUN_TEST(write);
  RUN_TEST(read);
  tear_down();
#else
  printf("Test disabled\n");
#endif /* DISABLED */
  return 0;
}